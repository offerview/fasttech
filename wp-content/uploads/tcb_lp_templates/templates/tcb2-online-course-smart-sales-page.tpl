<div class="thrive-group-edit-config" style="display: none !important">__CONFIG_group_edit__{"jzwgm81q":{"name":"Benefit Icon","singular":"Benefit Icon"},"jzwgmj3c":{"name":"Benefit Ribbon","singular":"Benefit Ribbon"},"jzwgmvmo":{"name":"Benefit Title","singular":"Benefit Title"},"jzwgn7eg":{"name":"Benefit Text","singular":"Benefit Text"},"jzwgnhtk":{"name":"Benefit Box","singular":"Benefit Box"},"jzwhuqqb":{"name":"Benefit Column","singular":"Benefit Column"},"jzwhv2de":{"name":"Benefit Columns","singular":"Benefit Columns"},"jzwhwobn":{"name":"Testimonials 1 Title","singular":"Testimonials 1 Title"},"jzwhy5jn":{"name":"Testimonials 1 Text","singular":"Testimonials 1 Text"},"jzwhyhoa":{"name":"Testimonials 1 Name","singular":"Testimonials 1 Name"},"jzwhyvle":{"name":"Testimonials 1 Role","singular":"Testimonials 1 Role"},"jzwifsck":{"name":"Testimonials 1 Image","singular":"Testimonials 1 Image"},"jzwig1wr":{"name":"Testimonials 1 Pattern","singular":"Testimonials 1 Pattern"},"jzwigch7":{"name":"Testimonials 1 Name &amp;Role Column","singular":"Testimonials 1 Name &amp;Role Column"},"jzwihb6t":{"name":"Testimonials 1  Image &amp;Name Box","singular":"Testimonials 1  Image &amp;Name Box"},"jzwihq3f":{"name":"Testimonials 1  Text Box","singular":"Testimonials 1  Text Box"},"jzwii3v7":{"name":"Testimonials 1  Individual Box","singular":"Testimonials 1  Individual Box"},"jzwiilh0":{"name":"Testimonials 1  Odd Columns","singular":"Testimonials 1  Odd Columns"},"jzwijru4":{"name":"Module Title","singular":"Module Title"},"jzwik6vg":{"name":"Module Description","singular":"Module Description"},"jzwimoag":{"name":"Module Description Column","singular":"Module Description Column"},"jzwin12z":{"name":"Module Number","singular":"Module Number"},"jzwip1e5":{"name":"Module Columns","singular":"Module Columns"},"jzwiprst":{"name":"Module Box","singular":"Module Box"},"jzwirpjx":{"name":"Bonus Vertical Line","singular":"Bonus Vertical Line"},"jzwise3g":{"name":"Bonus Number","singular":"Bonus Number"},"jzwiswby":{"name":"Bonus Title","singular":"Bonus Title"},"jzwiu1m7":{"name":"Bonus Description","singular":"Bonus Description"},"jzwiuuku":{"name":"Bonus Box","singular":"Bonus Box"},"jzwiyfdj":{"name":"Bonus Column","singular":"Bonus Column"},"jzwiypg8":{"name":"Bonus Columns","singular":"Bonus Columns"},"jzwjkv2n":{"name":"Testimonials 2 Name","singular":"Testimonials 2 Name"},"jzwjlaqv":{"name":"Testimonials 2 Role","singular":"Testimonials 2 Role"},"jzwjlki9":{"name":"Testimonials 2 Image","singular":"Testimonials 2 Image"},"jzwjluh3":{"name":"Testimonials 2 Pattern","singular":"Testimonials 2 Pattern"},"jzwjmdiw":{"name":"Testimonials 2 Name &amp; Role Column","singular":"Testimonials 2 Name &amp; Role Column"},"jzwjmr2f":{"name":"Testimonials 2 Image &amp; Name Box","singular":"Testimonials 2 Image &amp; Name Box"},"jzwjnae7":{"name":"Testimonials 2 Title","singular":"Testimonials 2 Title"},"jzwjnkvb":{"name":"Testimonials 2 Text","singular":"Testimonials 2 Text"},"jzwjnut3":{"name":"Testimonials 2 Text Box","singular":"Testimonials 2 Text Box"},"jzwjo5fj":{"name":"Testimonials 2 Individual Box","singular":"Testimonials 2 Individual Box"},"jzwjopn3":{"name":"Testimonials 2 Odd Columns","singular":"Testimonials 2 Odd Columns"},"jzwl2lsa":{"name":"1st Instance Pricing Table Icons","singular":"1st Instance Pricing Table 1 Icons"},"jzwl37p0":{"name":"1st Instance Pricing Table Pattern","singular":"1st Instance Pricing Table Pattern"},"jzwl3jno":{"name":"1st Instance Pricing Table Price","singular":"1st Instance Pricing Table Price"},"jzwl3vj0":{"name":"1st Instance Pricing Table Currency","singular":"1st Instance Pricing Table Currency"},"jzwl45el":{"name":"1st Instance Pricing Table  Title","singular":"1st Instance Pricing Table Title"},"jzwl4lwe":{"name":"1st Instance Pricing Table Button","singular":"1st Instance Pricing Table Button"},"jzwl6565":{"name":"1st Instance Pricing Table List Text","singular":"1st Instance Pricing Table List Text"},"jzwl6vlq":{"name":"1st Instance Pricing Table List Icons","singular":"1st Instance Pricing Table List Icons"},"jzwl77zp":{"name":"1st Instance Pricing Table List Item","singular":"1st Instance Pricing Table List Item"},"jzwl7qja":{"name":"1st Instance Pricing Table Lists Box","singular":"1st Instance Pricing Table Lists Box"},"jzwl8252":{"name":"1st Instance Pricing Table Summary ","singular":"1st Instance Pricing Table Summary "},"jzwl8i79":{"name":"1st Instance Pricing Table Title Box","singular":"1st Instance Pricing Table Title Box"},"jzwl8slr":{"name":"1st Instance Pricing Table Box","singular":"1st Instance Pricing Table Box"},"jzwl9myh":{"name":"2nd Instance Pricing Table Icons","singular":"2nd Instance Pricing Table Icons"},"jzwla73x":{"name":"2nd Instance Pricing Table Pattern","singular":"2nd Instance Pricing Table Pattern"},"jzwlast5":{"name":"2nd Instance Pricing Table Title","singular":"2nd Instance Pricing Table Title"},"jzwlb6q1":{"name":"2nd Instance Pricing Table Summary","singular":"2nd Instance Pricing Table Summary"},"jzwlbfyu":{"name":"2nd Instance Pricing Table Title Box","singular":"2nd Instance Pricing Table Title Box"},"jzwlbugq":{"name":"2nd Instance Pricing Table Price","singular":"2nd Instance Pricing Table Price"},"jzwlch73":{"name":"2nd Instance Pricing Table Currency","singular":"2nd Instance Pricing Table Currency"},"jzwlcuzb":{"name":"2nd Instance Pricing Table Period","singular":"2nd Instance Pricing Table Period"},"jzwldfjw":{"name":"2nd Instance Pricing Table List Text","singular":"2nd Instance Pricing Table List Text"},"jzwldz7j":{"name":"2nd Instance Pricing Table List Icons","singular":"2nd Instance Pricing Table List Icons"},"jzwledji":{"name":"2nd Instance Pricing Table List Items","singular":"2nd Instance Pricing Table List Items"},"jzwleqrz":{"name":"2nd Instance Pricing Table Button","singular":"2nd Instance Pricing Table Button"},"jzwlf27m":{"name":"2nd Instance Pricing Table List Box","singular":"2nd Instance Pricing Table List Box"},"jzwlfckf":{"name":"2nd Instance Pricing Table Box","singular":"2nd Instance Pricing Table Box"},"jzwlfr20":{"name":"2nd Instance Pricing Table Column","singular":"2nd Instance Pricing Table Column"},"jzwlgcfd":{"name":"1st Instance Pricing Table Column","singular":"1st Instance Pricing Table Column"},"jzwlolva":{"name":"Testimonials 3 Name","singular":"Testimonials 3 Name"},"jzwloz7n":{"name":"Testimonials 3 Role","singular":"Testimonials 3 Role"},"jzwlp8nf":{"name":"Testimonials 3 Image","singular":"Testimonials 3 Image"},"jzwlpi5g":{"name":"Testimonials 3 Pattern","singular":"Testimonials 3 Pattern"},"jzwlptzv":{"name":"Testimonials 3 Name &amp; Role Column","singular":"Testimonials 3 Name &amp; Role Column"},"jzwlq5x7":{"name":"Testimonials 3 Image &amp; Name Box","singular":"Testimonials 3 Image &amp; Name Box"},"jzwlqlos":{"name":"Testimonials 3 Title","singular":"Testimonials 3 Title"},"jzwlqym5":{"name":"Testimonials 3 Text","singular":"Testimonials 3 Text"},"jzwlr88z":{"name":"Testimonials 3 Text Box","singular":"Testimonials 3 Text Box"},"jzwlrirz":{"name":"Testimonials 3 Individual Box","singular":"Testimonials 3 Individual Box"},"jzwlrtym":{"name":"Testimonials 3 Odd Columns","singular":"Testimonials 3 Odd Columns"},"jzwlskyu":{"name":"Toggle Title","singular":"Toggle Title"},"jzwlsytw":{"name":"Toggle Text","singular":"Toggle Text"},"jzwlt8v0":{"name":"Toggle Element","singular":"Toggle Element"},"jzwlvkil":{"name":"Toggle Column","singular":"Toggle Column"},"jzwlvtwd":{"name":"Toggle Columns","singular":"Toggle Columns"}}__CONFIG_group_edit__</div><div class="thrv_wrapper thrv-page-section" data-inherit-lp-settings="1" data-css="tve-u-16cdc668863" style="">
	<div class="tve-page-section-out" data-css="tve-u-16cdc5fda65"></div>
	<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-16cdc61feee" style=""><div class="thrv_wrapper thrv_text_element" data-tag="h1" data-css="tve-u-16cdc6710eb"><h1 class="" style="text-align: center;">The Most Enticing <strong>Benefit-Driven</strong> Headline You Can Write Goes Here</h1></div><div class="thrv_responsive_video thrv_wrapper" data-type="youtube" data-url="https://www.youtube.com/watch?v=r7tjZJw_0M4" data-overlay="1" data-css="tve-u-16b4bec741d">
	

	<div class="tve_responsive_video_container">
		<div class="video_overlay video_overlay_image" data-width="770" data-height="432" style="background-image: url(&quot;{tcb_lp_base_url}/css/images/OC_Vid_thumbnail.jpg&quot;); background-repeat: no-repeat; background-size: cover; background-position: center center;"><span class="overlay_play_button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><title>play</title><path fill="#fff" d="M18.659 4.98c-0.889-1.519-2.12-2.75-3.593-3.614l-0.047-0.025q-2.298-1.341-5.020-1.341t-5.019 1.341c-1.52 0.889-2.751 2.12-3.614 3.593l-0.025 0.047q-1.341 2.298-1.341 5.020t1.341 5.020c0.889 1.519 2.12 2.75 3.593 3.614l0.047 0.025q2.298 1.341 5.020 1.341t5.020-1.341c1.519-0.889 2.75-2.12 3.614-3.593l0.025-0.047q1.341-2.298 1.341-5.020t-1.341-5.020zM15 10.716l-7.083 4.167c-0.118 0.074-0.262 0.117-0.416 0.117-0 0-0 0-0.001 0h0c-0.153-0.002-0.296-0.040-0.422-0.107l0.005 0.002q-0.417-0.247-0.417-0.729v-8.333q0-0.482 0.417-0.729 0.43-0.234 0.833 0.013l7.084 4.167q0.416 0.234 0.416 0.716t-0.416 0.716z"></path></svg></span></div>
	<iframe data-code="r7tjZJw_0M4" data-provider="youtube" src="https://www.youtube.com/embed/r7tjZJw_0M4?rel=1&amp;modestbranding=0&amp;controls=1&amp;showinfo=1&amp;fs=1&amp;wmode=transparent&amp;start=undefined" data-src="https://www.youtube.com/embed/r7tjZJw_0M4?rel=1&amp;modestbranding=0&amp;controls=1&amp;showinfo=1&amp;fs=1&amp;wmode=transparent&amp;start=undefined" frameborder="0" allowfullscreen=""></iframe></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685be6002c" style="margin-bottom: 0px !important;">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv-columns" data-css="tve-u-1685bea876e"><div class="tcb-flex-row tcb-resized tcb--cols--2" data-css="tve-u-1685bea84b7"><div class="tcb-flex-col" data-css="tve-u-1685beca9a6" style=""><div class="tcb-col" data-css="tve-u-16cdd2b7da4"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687fb1a807"><p style="">Learn to be able to achieve this incredible goal… even if you have this [major objection]!</p></div></div></div><div class="tcb-flex-col" data-css="tve-u-1685beca9b9" style=""><div class="tcb-col"><div class="tcb-clear" data-css="tve-u-16904cefb06"><div class="thrv_wrapper thrv-button tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs" style="" data-tcb_hover_state_parent="" data-css="tve-u-16cd87a7ad8" data-button-style="tcb-global-button-tpl_jzvb5fzs">
	<a href="#Pricing" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
		<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext">Enroll Now</span></span>
	</a>
</div></div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzvcyzi8" data-css="tve-u-16cd8a48aaa" style="">
<div class="tve-page-section-out tcb-global-section-tpl_jzvcyzi8-out" data-css="tve-u-16cd8a48ac0"></div>
<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzvcyzi8-in" data-css="tve-u-16cd8a48ab5" style=""><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-16cdd271ccd" style="">
<div class="tve-content-box-background" data-css="tve-u-16d49074800"></div>
<div class="tve-cb" data-css="tve-u-16afd27ad3a"><div class="thrv_wrapper thrv-columns" data-css="tve-u-16afdb05f30"><div class="tcb-flex-row tcb--cols--2 tcb-resized v-2 tcb-medium-wrap" data-css="tve-u-170e748c491" style=""><div class="tcb-flex-col" data-css="tve-u-170e74261fc" style=""><div class="tcb-col" data-css="tve-u-16cdd2ccabe"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687fb2d2dd" data-tag="h4"><h4 class="" data-css="tve-u-170e3019144">Course Enrollments close on&nbsp;<strong>July 25th 2019</strong></h4></div></div></div><div class="tcb-flex-col" data-css="tve-u-170e7426213" style=""><div class="tcb-col"><div class="tcb-clear" data-css="tve-u-16afdb63082"><div class="thrv_wrapper thrv_countdown_timer thrv-countdown_timer_plain tve_clearfix tve_red tve_countdown_2" data-date="2019-10-25" data-hour="22" data-min="14" data-timezone="+00:00" data-css="tve-u-1685bfa59bf" style="">
<div class="sc_timer_content tve_clearfix tve_block_center">
<div class="tve_t_day tve_t_part">
<div class="t-digits" style=""><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd85d89fd">Days</div>
</div>
<div class="tve_t_hour tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8a28883">Hours</div>
</div>
<div class="tve_t_min tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8a29a53">Minutes</div>
</div>
<div class="tve_t_sec tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8a2acac">Seconds</div>
</div>
<div class="tve_t_text" style="display: none;"></div>
</div>
</div></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad" data-css="tve-u-170e2ff2527" style="">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element" data-tag="h2" data-css="tve-u-168603256ce"><h2 class="" data-css="tve-u-16cdc6d1c80" style="text-align: center;">This subheading should target <strong>a pain point</strong> that your online course can solve</h2></div><div class="thrv_wrapper thrv_text_element tve-froala fr-box" data-css="tve-u-1685c0ee37d"><p style="text-align: center;">The format for this section is known as <a class="tve-froala" href="https://thrivethemes.com/pas-copywriting-technique/" style="outline: none;" target="_blank">PAS: Pain, Agitation, Solution.</a> Start by identifying your customer’s <strong>pain</strong>. Show them that <em>you understand their problem</em>.</p></div><div class="thrv_wrapper thrv-columns" data-css="tve-u-1685c0e6c3b"><div class="tcb-flex-row tcb--cols--2 v-2 tcb-resized" data-css="tve-u-1685c0e6834"><div class="tcb-flex-col" data-css="tve-u-16cdd398a82" style=""><div class="tcb-col" data-css="tve-u-1685c14ddde"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-16cdd2d3630"><p style="">Don’t rush to introduce your course, but instead be really specific when you describe exactly how it feels to have this problem. You want your reader to start nodding and thinking “Yes, that’s exactly my problem!</p><p style="">Once you’ve really made the pain clear, it’s time to agitate it. Indulge in how this problem gets worse. Why is it frustrating? Why does it feel so awful? What is it that makes the problem so problematic?</p><p style="">After a few short paragraphs, you should have your readers 100% focused on the issue and how bad it is.</p></div></div></div><div class="tcb-flex-col" data-css="tve-u-16cdd398b63" style=""><div class="tcb-col" data-css="tve-u-1685c1512d6"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1685c127b2d"><p style="">If you are writing a long-form sales page, this is the section that would grow the most. You could expand this into a blog post format, with a content pattern that explores the customer’s pain even further. Add images, icons, subheadings, quotes and more.</p><p style="">If you’ve written this section well, your visitors should be wanting a solution by the end of it. They’ll be hoping that you’re about to turn things around and relieve their pain for them.</p><p style="">And you are…</p></div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168604cedc5" data-inherit-lp-settings="1">
<div class="tve-page-section-out"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1685c8a1093"><div class="thrv_wrapper thrv_text_element" data-tag="h2" data-css="tve-u-1685c0e8154"><h2 class="" data-css="tve-u-1685c8db5fd" style="text-align: center;">Introduce Your Online Course Title</h2></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685c40e6db">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1685c3f3ad3"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-mobile-hidden tcb-tablet-hidden" data-css="tve-u-1685c3fa7aa">
<div class="tve-content-box-background" data-css="tve-u-1685c39a1b8"></div>
<div class="tve-cb" data-css="tve-u-1685c3f91d9"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in" data-css="tve-u-1685c49fd32" data-float="1" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-1685c541206"></div>
<div class="tve-cb" data-css="tve-u-1685c4ab443"></div>
</div></div>
</div><div class="tcb-clear" data-css="tve-u-1685c4264fe"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685c47be13" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-1685c482fad"></div>
<div class="tve-cb" data-css="tve-u-1685c48b849"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in" data-css="tve-u-1685c83555b" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-1685c84a2b9"></div>
<div class="tve-cb" data-css="tve-u-1685c8364fb"></div>
</div></div>
</div></div><div class="tcb-clear" data-css="tve-u-1687af79b5d"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685c453c6d">
<div class="tve-content-box-background" data-css="tve-u-1685c45a4fa"></div>
<div class="tve-cb" data-css="tve-u-1685c460750"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in" data-css="tve-u-1685c8113a8" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-1685c8250de"></div>
<div class="tve-cb" data-css="tve-u-1685c812901"></div>
</div></div>
</div></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685c397c76">
<div class="tve-content-box-background" data-css="tve-u-1685c3d9434"></div>
<div class="tve-cb" data-css="tve-u-1685c3a36e1"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in" data-css="tve-u-1685c6d0f01" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-1685c7f6e74"></div>
<div class="tve-cb" data-css="tve-u-1685c6dab3b"></div>
</div></div>
</div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzvcxdvy" data-css="tve-u-16cd8a366f5" data-inherit-lp-settings="1">
<div class="tve-page-section-out tcb-global-section-tpl_jzvcxdvy-out" data-css="tve-u-16cd8a3670e"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1000 78" preserveAspectRatio="none" decoration-type="waves" class="svg-shape-top" data-css="tve-u-1685c26698e"><path d="M0,0v60c51.1-27,123.1-42,216-45c122-4,207.4,27.3,443,38c72.6,3.3,186.3-4.4,341-23V0H0z"></path><path opacity="0.6" d="M1,1v60c23.1-17,81.1-27,174-30c122-4,169.4,32.3,405,43c72.6,3.3,213-11,421-43V1H1z"></path><path opacity="0.2" d="M1,0v62c17.8-9,73.1-15,166-18c122-4,188,18,366,18c62,0,147.7-9,314-9     c32.1,0,83.4,6,154,18V0H1z"></path></svg></div>
<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzvcxdvy-in" data-css="tve-u-16cd8a36703"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1685c9295f6"><p data-css="tve-u-1687b15b7ca" style="text-align: center;"><!--StartFragment-->Here you want to write a short paragraph that quickly explains exactly <em>what</em> your online course is and how it’s the perfect solution to the problems you’ve been talking about in the previous section. Keep it short, but provide the essential information they need to be able to make sense of what’s coming. For example, make sure they know that it is an <em>online</em> course designed to teach them how to achieve their goal<!--EndFragment--></p></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-1685c994114" data-tve-scroll="{&quot;disabled&quot;:[],&quot;top&quot;:0,&quot;mode&quot;:&quot;sticky&quot;,&quot;end&quot;:&quot;element&quot;,&quot;el_id&quot;:&quot;Pricing&quot;}" data-inherit-lp-settings="1">
<div class="tve-page-section-out" data-css="tve-u-1685c9bfca0"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1685c9ec5f0"><div class="thrv_wrapper thrv-columns" data-css="tve-u-168601a5d61"><div class="tcb-flex-row tcb--cols--3 tcb-resized tcb-medium-no-wrap tcb-mobile-no-wrap" data-css="tve-u-1686017c666"><div class="tcb-flex-col" data-css="tve-u-16861227198" style=""><div class="tcb-col"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168602691e0"><p data-css="tve-u-1686026d6b6" style=""><strong>Special offer! Enroll now and get a <span data-css="tve-u-16860271978" style="color: var(--tcb-color-1); font-family: Muli; font-weight: 800;">35% discount !</span></strong></p></div></div></div><div class="tcb-flex-col" data-css="tve-u-168612271db" style=""><div class="tcb-col"><div class="thrv_wrapper thrv-divider tcb-mobile-hidden" data-style="tve_sep-1" data-thickness="1" data-color="rgba(8, 8, 18, 0.1)" data-css="tve-u-1686121a548">
<hr class="tve_sep tve_sep-1">
</div></div></div><div class="tcb-flex-col" data-css="tve-u-1686122721e" style=""><div class="tcb-col"><div class="tcb-clear" data-css="tve-u-1688442a1d1"><div class="thrv_wrapper thrv-button tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs" style="" data-tcb_hover_state_parent="" data-button-style="" data-css="tve-u-16cd875e189">
<a href="#Pricing" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" rel="">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext"><strong>Enroll Now</strong></span></span>
</a>
</div></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168602fef0f" data-inherit-lp-settings="1">
<div class="tve-page-section-out" data-css="tve-u-168602d2e4a" data-clip-id="38975a491a3ca"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1686030115d"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-168603280cf"><h3 data-css="tve-u-1686030ea3f" style="text-align: center;" class="">Here’s What You’ll Get When You Sign Up</h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-16860329d81"><p data-css="tve-u-16860558abc" style="text-align: center;"><!--StartFragment-->This paragraph here is to explain how your online course works. Tell them what exactly will happen after they sign up. Do they get access to all of it straight away? How much time will it take them to complete the course? What result will they get at the end?<!--EndFragment--></p></div><div class="thrv_wrapper thrv-columns dynamic-group-jzwhv2de" data-css="tve-u-168604259df"><div class="tcb-flex-row tcb--cols--2 v-2" data-css="tve-u-168705597ae"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwhuqqb" data-css="tve-u-16cdcbca79c"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgnhtk" data-css="tve-u-1687096b7b4" style="">
<div class="tve-content-box-background" data-css="tve-u-1686033edb9" data-clip-id="8aaff76950c48"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168603862c0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgmj3c" data-css="tve-u-1687b17d503">
<div class="tve-content-box-background" data-css="tve-u-1686046e138" data-clip-id="f7919809fcbe6"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-bottom-f7919809fcbe6" class="decoration-clip clip-path-bottom" clipPathUnits="objectBoundingBox" data-screen="" decoration-type="pointer" pointer-width="100" pointer-height="12" style="" data-inverted="true"><polygon points="0 0, 0 1, 0 1, 0.5 0.88, 1 1, 1 1, 1 0"></polygon></clipPath></defs></svg></div>
<div class="tve-cb" data-css="tve-u-1688009d697"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwgm81q" data-css="tve-u-1687b17fc83"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-heart-light" data-name="">
<path d="M230.1 307.9c5.5 5.5 14.3 5.5 19.8 0l84.7-85.6c24.6-24.9 23.2-66.1-4.3-89.1-13.9-11.6-47.7-28.5-90.2 14.5-42.6-43-76.4-26.1-90.2-14.5-27.5 23-28.9 64.2-4.3 89.1l84.5 85.6zm-59.8-150.1c13.7-11.5 31.2-3.4 38.4 3.7l31.4 31.7 31.4-31.7c7.1-7.2 24.6-15.2 38.4-3.7 14.4 12 12.3 31.6 2.1 42l-72 72.6-71.8-72.6c-10.3-10.4-12.3-30 2.1-42zM448 392V24c0-13.3-10.7-24-24-24H80C35.8 0 0 35.8 0 80v352c0 44.2 35.8 80 80 80h356c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12h-3.3c-4-20.2-3.2-49.7.4-65.8 8.7-3.6 14.9-12.2 14.9-22.2zm-43.7 88H80c-64 0-64-64 0-64h324.3c-2.9 18.8-3.1 43.6 0 64zm11.7-96H80c-18 0-34.6 6-48 16V80c0-26.5 21.5-48 48-48h336v352z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgmvmo" data-css="tve-u-1686039176f" data-tag="h6"><h6 class="" data-css="tve-u-1686035397f" style=""><strong>Huge Benefit 1</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgn7eg" data-css="tve-u-16860344f75"><p data-css="tve-u-1686040fa93" style="">Insert some text here that summarises this specific benefit that they can get from joining your course</p></div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwhuqqb" data-css="tve-u-16cdcbcb8cc"><div class="tcb-clear" data-css="tve-u-16860429006"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgnhtk" data-css="tve-u-16860429002" style="">
<div class="tve-content-box-background" data-css="tve-u-1686033edb9" data-clip-id="2642e48aa2c97"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168603862c0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgmj3c" data-css="tve-u-1688009f30a">
<div class="tve-content-box-background" data-css="tve-u-16860473c33" data-clip-id="0861a354ed872"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-bottom-0861a354ed872" class="decoration-clip clip-path-bottom" clipPathUnits="objectBoundingBox" data-screen="" decoration-type="pointer" pointer-width="100" pointer-height="12" style="" data-inverted="true"><polygon points="0 0, 0 1, 0 1, 0.5 0.88, 1 1, 1 1, 1 0"></polygon></clipPath></defs></svg></div>
<div class="tve-cb" data-css="tve-u-1688009feaa"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwgm81q" data-css="tve-u-1687b185b5d"><svg class="tcb-icon" viewBox="0 0 512 512" data-id="icon-badge-check-light" data-name="">
<path d="M512 256c0-35.496-19.411-68.153-49.598-85.502 9.075-33.611-.289-70.424-25.383-95.517-25.092-25.094-61.902-34.458-95.518-25.384C324.153 19.411 291.496 0 256 0s-68.153 19.411-85.502 49.598c-33.612-9.076-70.425.291-95.518 25.384-25.094 25.093-34.458 61.905-25.383 95.517C19.411 187.847 0 220.504 0 256s19.411 68.153 49.598 85.502c-9.075 33.611.289 70.424 25.383 95.519 26.511 26.507 63.455 34.154 95.532 25.406C187.865 492.6 220.514 512 256 512s68.135-19.4 85.487-49.573c32.709 8.92 69.471.651 95.532-25.407 25.094-25.094 34.458-61.906 25.383-95.518C492.589 324.153 512 291.496 512 256zm-91.145 68.29c5.346 11.778 29.582 54.057-6.463 90.102-28.863 28.861-57.547 21.24-90.103 6.464C319.745 432.959 306.99 480 256 480c-52.106 0-64.681-49.533-68.29-59.145-32.611 14.801-61.35 22.286-90.103-6.463-36.746-36.747-10.826-80.49-6.463-90.103C79.042 319.745 32 306.99 32 256c0-52.106 49.533-64.681 59.145-68.29-5.346-11.778-29.582-54.057 6.463-90.102 36.836-36.833 80.756-10.706 90.103-6.464C192.255 79.041 205.01 32 256 32c52.106 0 64.681 49.533 68.29 59.145 11.769-5.342 54.059-29.58 90.103 6.464 36.746 36.745 10.826 80.489 6.463 90.102C432.958 192.255 480 205.01 480 256c0 52.106-49.533 64.681-59.145 68.29zm-32.404-138.802L207.971 364.52c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.971.068l68.976 69.533 163.441-162.129c4.705-4.667 12.303-4.637 16.97.068l8.452 8.52c4.666 4.703 4.635 12.301-.07 16.969z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgmvmo" data-css="tve-u-1686039176f" data-tag="h6"><h6 class="" data-css="tve-u-1686035397f" style=""><strong>Huge Benefit 2</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgn7eg" data-css="tve-u-16860344f75"><p data-css="tve-u-1686040fa93" style="">Insert some text here that summarises this specific benefit that they can get from joining your course</p></div></div>
</div></div></div></div></div></div><div class="thrv_wrapper thrv-columns dynamic-group-jzwhv2de" data-css="tve-u-16860448142"><div class="tcb-flex-row tcb--cols--2" data-css="tve-u-1687076d12a"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwhuqqb" data-css="tve-u-16cdcbccb9b"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgnhtk" data-css="tve-u-1687096c1e4" style="">
<div class="tve-content-box-background" data-css="tve-u-1686033edb9" data-clip-id="27abdc21c88c5"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168603862c0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgmj3c" data-css="tve-u-168800a1635">
<div class="tve-content-box-background" data-css="tve-u-168604767be" data-clip-id="2c134f3ef46cf"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-bottom-2c134f3ef46cf" class="decoration-clip clip-path-bottom" clipPathUnits="objectBoundingBox" data-screen="" decoration-type="pointer" pointer-width="100" pointer-height="12" style="" data-inverted="true"><polygon points="0 0, 0 1, 0 1, 0.5 0.88, 1 1, 1 1, 1 0"></polygon></clipPath></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168800a2184"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwgm81q" data-css="tve-u-1687b18eb87"><svg class="tcb-icon" viewBox="0 0 512 512" data-id="icon-chess-queen-light" data-name="">
<path d="M436 512H76c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h360c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zM255.579 32c-13.234 0-24 10.766-24 24s10.766 24 24 24 24-10.766 24-24-10.766-24-24-24m0-32c30.928 0 56 25.072 56 56s-25.072 56-56 56-56-25.072-56-56 25.072-56 56-56zm81.579 160h-8.319c-4.533 9.593-24.68 49.231-72.839 49.231-48.147 0-68.317-39.664-72.839-49.231h-8.315c-1.485 21.883-7.446 69.206-62.846 69.206-26.644 0-46.348-15.694-61.685-37.576l-6.854 3.617L133.507 416h244.986l90.046-220.753-6.777-3.577c-9.187 13.212-29.262 37.536-61.762 37.536-55.422 0-61.374-47.614-62.842-69.206m19.161-32c6.494 0 11.812 5.172 11.995 11.664 1.062 37.738 2.973 57.542 31.686 57.542 21.318 0 35.449-22.285 44.065-37.802 3.161-5.693 10.305-7.82 16.082-4.77l39.357 20.773a12 12 0 0 1 5.51 15.145L400 448H112L6.986 190.552a12 12 0 0 1 5.51-15.145l39.179-20.679c6.482-3.421 13.147-.165 15.899 4.453 10.608 17.8 23.735 38.025 44.425 38.025 28.753 0 30.635-19.898 31.688-57.539.181-6.493 5.5-11.667 11.995-11.667h41.005c5.175 0 9.754 3.328 11.388 8.238 8.89 26.709 26.073 40.992 47.925 40.992s39.034-14.283 47.925-40.992c1.634-4.911 6.213-8.238 11.388-8.238h41.006z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgmvmo" data-css="tve-u-1686039176f" data-tag="h6"><h6 class="" data-css="tve-u-1686035397f" style=""><strong>Huge Benefit 3</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgn7eg" data-css="tve-u-16860344f75"><p data-css="tve-u-1686040fa93" style="">Insert some text here that summarises this specific benefit that they can get from joining your course</p></div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwhuqqb" data-css="tve-u-16cdcbcdd89"><div class="tcb-clear" data-css="tve-u-16860429c28"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgnhtk" data-css="tve-u-1687096cb85" style="">
<div class="tve-content-box-background" data-css="tve-u-1686033edb9" data-clip-id="2c647262abc03"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168603862c0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwgmj3c" data-css="tve-u-168603a2018">
<div class="tve-content-box-background" data-css="tve-u-168604793ef" data-clip-id="7040c63b05aa6"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-bottom-7040c63b05aa6" class="decoration-clip clip-path-bottom" clipPathUnits="objectBoundingBox" data-screen="" decoration-type="pointer" pointer-width="100" pointer-height="12" style="" data-inverted="true"><polygon points="0 0, 0 1, 0 1, 0.5 0.88, 1 1, 1 1, 1 0"></polygon></clipPath></defs></svg></div>
<div class="tve-cb" data-css="tve-u-168603a949e"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwgm81q" data-css="tve-u-168603deab4"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-user-graduate-light" data-name="">
<path d="M319.4 320.6L224 400l-95.4-79.4C110.2 321.4 0 336.1 0 464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48 0-127.9-110.1-142.6-128.6-143.4zM208 480H48c-8.8 0-16-7.2-16-16 0-99.6 84.1-109.9 86.4-110.3l89.6 74.6V480zm208-16c0 8.8-7.2 16-16 16H240v-51.7l89.6-74.6c2.3.4 86.4 10.7 86.4 110.3zM13.2 100l6.8 2v37.6c-7 4.2-12 11.5-12 20.3 0 8.4 4.6 15.4 11.1 19.7L3.5 242c-1.7 6.9 2.1 14 7.6 14h41.8c5.5 0 9.3-7.1 7.6-14l-15.6-62.3C51.4 175.4 56 168.4 56 160c0-8.8-5-16.1-12-20.3v-30.5L90.7 123C84 139.4 80 157.2 80 176c0 79.5 64.5 144 144 144s144-64.5 144-144c0-18.8-4-36.6-10.7-53l77.5-23c17.6-5.2 17.6-34.8 0-40L240.9 2.5C235.3.8 229.7 0 224 0s-11.3.8-16.9 2.5L13.2 60c-17.6 5.2-17.6 34.8 0 40zM224 288c-61.8 0-112-50.2-112-112 0-15.7 3.7-30.3 9.6-43.8l85.5 25.4c14.8 4.4 27.2 2 33.8 0l85.5-25.4c5.9 13.5 9.6 28.2 9.6 43.8 0 61.8-50.2 112-112 112zm-7.8-254.9c.8-.2 7.3-2.4 15.6 0l158 46.9-158 46.9c-.8.2-7.3 2.4-15.6 0L58.2 80l158-46.9z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgmvmo" data-css="tve-u-1686039176f" data-tag="h6"><h6 class="" data-css="tve-u-1686035397f" style=""><strong>Huge Benefit 4</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwgn7eg" data-css="tve-u-16860344f75"><p data-css="tve-u-1686040fa93" style="">Insert some text here that summarises this specific benefit that they can get from joining your course</p></div></div>
</div></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168604d64ed">
<div class="tve-page-section-out" data-css="tve-u-168604e0deb"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-168604e56fa"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-1687a2f0c50"><h3 style="text-align: center;" class="">Here’s what people are saying about the course</h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687b239ecf"><p style="text-align: center;">This paragraph is optional, but you might want to use it to introduce your upcoming testimonials. Since you’ve just introduced your course, they should focus on <em>how the course helped customers to obtain a huge benefit.&nbsp;</em>If you haven’t got great testimonials yet, then check out our the <a href="https://thrivethemes.com/get-raving-testimonials-for-your-online-course/" target="_blank">secret to getting raving testimonials for your online course</a></p></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" style="" data-css="tve-u-1602b1ebd26" data-ct-name="Resume Clients" data-ct="testimonial-7342" data-element-name="Testimonial">
<div class="tve-content-box-background"></div>
<div class="tve-cb tve_empty_dropzone" data-css="tve-u-16cdcc0fe5a"><div class="thrv_wrapper thrv-columns" data-css="tve-u-16860547721"><div class="tcb-flex-row tcb-row-reversed-tablet tcb-row-reversed-mobile tcb-desktop-no-wrap v-2 tcb--cols--3" data-css="tve-u-16870ae36ee"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwiilh0" data-css="tve-u-168605c3908"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwii3v7" data-css="tve-u-1688010799d">
<div class="tve-content-box-background" data-css="tve-u-168800e3dde" data-clip-id="72d4107a9f883"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-mobile-top-72d4107a9f883" class="decoration-clip clip-path-mobile-top" clipPathUnits="objectBoundingBox" data-screen="mobile-" pointer-width="100" pointer-height="25" decoration-type="none"><polygon points="0 0, 0 1, 1 1, 1 0"></polygon></clipPath></defs></svg></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwihb6t" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwig1wr" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16afddca955"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwifsck" data-css="tve-u-16cdcc5e03f"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31210" alt="" width="104" height="105" title="Online_Course_person1_03" data-id="31210" src="{tcb_lp_base_url}/css/images/Online_Course_person1_03.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwigch7" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyhoa" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd86733ff" style=""><strong><strong><strong>Marie Fisher</strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyvle" data-css="tve-u-16afdd60367"><p data-css="tve-u-16cdd8a60d8" style="">Developer</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwihq3f" data-css="tve-u-168800e7098">
<div class="tve-content-box-background" data-css="tve-u-168711a3ff6" data-clip-id="93303b9e8564a"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd6a6c48"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhwobn" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhy5jn" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->Since these 3 testimonials are just after you’ve introduced your course, they should focus on <em>how the course helped customers to obtain a huge benefit.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col" data-css="tve-u-1687b353150"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwii3v7" data-css="tve-u-1687091a411">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwihb6t" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwig1wr" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16afddca955"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwifsck" data-css="tve-u-16cdcc5e03f"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31282" alt="" width="104" height="104" title="Online_Course_person2_03" data-id="31282" src="{tcb_lp_base_url}/css/images/Online_Course_person2_03.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwigch7" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyhoa" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd8674859" style=""><strong><strong><strong><strong>Carl Numan</strong></strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyvle" data-css="tve-u-16afdd60367"><p data-css="tve-u-16cdd8a60dc" style="">Hotel Manager</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwihq3f" data-css="tve-u-168800e8e1f">
<div class="tve-content-box-background" data-css="tve-u-16860577a64" data-clip-id="237bc21e3ff6"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd6a80ba"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhwobn" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhy5jn" data-css="tve-u-168605f888e"><p data-css="tve-u-168611a00dd" style=""><!--StartFragment-->Since these 3 testimonials are just after you’ve introduced your course, they should focus on <em>how the course helped customers to obtain a huge benefit.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwiilh0" data-css="tve-u-168605c5931"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwii3v7" data-css="tve-u-1687b4daac8">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwihb6t" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwig1wr" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16afddca955"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwifsck" data-css="tve-u-16cdcc5e03f"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31290" alt="" width="128" height="128" title="Online_Course_person3_03" data-id="31290" src="{tcb_lp_base_url}/css/images/Online_Course_person3_03.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwigch7" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyhoa" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd8675eba" style=""><strong><strong><strong><strong><strong>Gary Howard</strong></strong></strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhyvle" data-css="tve-u-16afdd60367"><p data-css="tve-u-16cdd8a60de" style="">Photographer</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwihq3f" data-css="tve-u-1688037c1e9">
<div class="tve-content-box-background" data-css="tve-u-168612c0361" data-clip-id="ff8bdaa09b9ab"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd6a933b"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhwobn" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwhy5jn" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->Since these 3 testimonials are just after you’ve introduced your course, they should focus on <em>how the course helped customers to obtain a huge benefit.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzvc9xyj" data-css="tve-u-16cd8c5d5d8">
<div class="tve-page-section-out tcb-global-section-tpl_jzvc9xyj-out" data-css="tve-u-16cd8c5d698"></div>
<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzvc9xyj-in" data-css="tve-u-16cd8c5d5e6"><div class="thrv_wrapper thrv_text_element" data-tag="h2" data-css="tve-u-168610e691a"><h2 style="text-align: center;" class="">Show them what’s in the course</h2></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687b1dc808"><p style="text-align: center;">Let your visitor know how many modules there are and what each one is about.</p></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiprst" data-css="tve-u-1687ab95d52">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv-columns dynamic-group-jzwip1e5" data-css="tve-u-1687b1e27be"><div class="tcb-flex-row tcb--cols--2 tcb-resized v-2" data-css="tve-u-1687ab9c726"><div class="tcb-flex-col" data-css="tve-u-1686129cfab" style=""><div class="tcb-col" data-css="tve-u-1687abc4936"><div class="tcb-clear" data-css="tve-u-1687b1e97fc"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687ab973a8">
<div class="tve-content-box-background" data-css="tve-u-16861121808"></div>
<div class="tve-cb" data-css="tve-u-1687aba6009"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwin12z" data-css="tve-u-1686113e149"><p data-css="tve-u-1687abb0e24" style="text-align: center;"><strong>1</strong></p></div></div>
</div></div></div></div><div class="tcb-flex-col" data-css="tve-u-1686129cfac" style=""><div class="tcb-col dynamic-group-jzwimoag" data-css="tve-u-168610f8fcf"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwijru4" data-css="tve-u-16880227683" data-tag="h6"><h6 class="" data-css="tve-u-168611a5738" style=""><strong>Module 1: Title Of Module Here</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwik6vg" data-css="tve-u-16861104c11"><p data-css="tve-u-1686119b904" style=""><!--StartFragment-->Explain exactly what this module will teach them, and use names of individual lessons if you can. You might want to include the number of lessons, the length of each lesson, or any other information. But make sure you focus on the benefits that the customer would get just by taking this module on it’s own<!--EndFragment--></p></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiprst" data-css="tve-u-1687b1fa1ee">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv-columns dynamic-group-jzwip1e5" data-css="tve-u-168610eff9d"><div class="tcb-flex-row tcb--cols--2 tcb-resized v-2" data-css="tve-u-1687b24e218"><div class="tcb-flex-col" data-css="tve-u-1686129cfab" style=""><div class="tcb-col" data-css="tve-u-16cdcd0584d"><div class="tcb-clear" data-css="tve-u-16afdb0f217"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687b21344e">
<div class="tve-content-box-background" data-css="tve-u-168612b30db"></div>
<div class="tve-cb" data-css="tve-u-1687b214800"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwin12z" data-css="tve-u-1686113e149"><p data-css="tve-u-1687b20a80e" style="text-align: center;"><strong>2</strong></p></div></div>
</div></div></div></div><div class="tcb-flex-col" data-css="tve-u-1686129cfac" style=""><div class="tcb-col dynamic-group-jzwimoag" data-css="tve-u-168612b6456"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwijru4" data-css="tve-u-1688025ed4c" data-tag="h6"><h6 class="" data-css="tve-u-168611a5738" style=""><strong>Module 2: Title Of Module Here</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwik6vg" data-css="tve-u-16861104c11"><p data-css="tve-u-1686119b904" style=""><!--StartFragment-->Explain exactly what this module will teach them, and use names of individual lessons if you can. You might want to include the number of lessons, the length of each lesson, or any other information. But make sure you focus on the benefits that the customer would get just by taking this module on it’s own<!--EndFragment--></p></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiprst" data-css="tve-u-168611fa9ad">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv-columns dynamic-group-jzwip1e5" data-css="tve-u-168610eff9d"><div class="tcb-flex-row tcb--cols--2 tcb-resized" data-css="tve-u-1687b24fce8"><div class="tcb-flex-col" data-css="tve-u-1686129cfab" style=""><div class="tcb-col" data-css="tve-u-16cdcd0842e"><div class="tcb-clear" data-css="tve-u-16afdb0fbc0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687b2178e0">
<div class="tve-content-box-background" data-css="tve-u-168613b0178"></div>
<div class="tve-cb" data-css="tve-u-1687b218433"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwin12z" data-css="tve-u-1686113e149"><p data-css="tve-u-1687b20caee" style="text-align: center;"><strong>3</strong></p></div></div>
</div></div></div></div><div class="tcb-flex-col" data-css="tve-u-1686129cfac" style=""><div class="tcb-col dynamic-group-jzwimoag" data-css="tve-u-168613b2cda"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwijru4" data-css="tve-u-1688027011e" data-tag="h6"><h6 class="" data-css="tve-u-168611a5738" style=""><strong>Module 3: Title Of Module Here</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwik6vg" data-css="tve-u-16861104c11"><p data-css="tve-u-1686119b904" style=""><!--StartFragment-->Explain exactly what this module will teach them, and use names of individual lessons if you can. You might want to include the number of lessons, the length of each lesson, or any other information. But make sure you focus on the benefits that the customer would get just by taking this module on it’s own<!--EndFragment--></p></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiprst" data-css="tve-u-168613d254e" data-float="1">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv-columns dynamic-group-jzwip1e5" data-css="tve-u-168802aa5aa"><div class="tcb-flex-row tcb--cols--2 tcb-resized v-2" data-css="tve-u-168610efc06"><div class="tcb-flex-col" data-css="tve-u-1686129cfab" style=""><div class="tcb-col" data-css="tve-u-16cdcd0a66f"><div class="tcb-clear" data-css="tve-u-16afdb108f2"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-16861114d7e">
<div class="tve-content-box-background" data-css="tve-u-168613bb622"></div>
<div class="tve-cb" data-css="tve-u-1686111a8c3"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwin12z" data-css="tve-u-1686113e149"><p data-css="tve-u-1686114ed08" style="text-align: center;"><strong>4</strong></p></div></div>
</div></div></div></div><div class="tcb-flex-col" data-css="tve-u-1686129cfac" style=""><div class="tcb-col dynamic-group-jzwimoag" data-css="tve-u-168613bd2d5"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwijru4" data-css="tve-u-168611b60b2" data-tag="h6"><h6 class="" data-css="tve-u-168611a5738" style=""><strong>Module 4: Title Of Module Here</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwik6vg" data-css="tve-u-16861104c11"><p data-css="tve-u-1686119b904" style=""><!--StartFragment-->Explain exactly what this module will teach them, and use names of individual lessons if you can. You might want to include the number of lessons, the length of each lesson, or any other information. But make sure you focus on the benefits that the customer would get just by taking this module on it’s own<!--EndFragment--></p></div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168613d0a4f">
<div class="tve-page-section-out" data-css="tve-u-168615220fe"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-168615342b4"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-16861574e52"><h3 data-css="tve-u-1686157fcbb" style="text-align: center;" class=""><strong>Bonus</strong></h3></div><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-16861575afb"><h3 data-css="tve-u-168615d3b7b" style="text-align: center;" class="">Now it’s time to introduce your Bonuses!</h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168610c1672"><p style="text-align: center;">What <em>else</em> is there in the course that you haven’t explained in your modules above? Even if it’s a regular part of your course, if you haven’t mentioned it at all on your sales page yet, then it’s time to introduce it in this section as a Bonus. If you’re not sure what to put here, then check out our article on <a href="https://thrivethemes.com/online-course-perceived-value/" target="_blank">6 great ways that you can increase the perceived value of your online course</a></p></div><div class="thrv_wrapper thrv-columns dynamic-group-jzwiypg8" data-css="tve-u-1686ff38d4c"><div class="tcb-flex-row tcb-desktop-no-wrap tcb--cols--3" data-css="tve-u-1686ff389f4"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwiyfdj"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiuuku tcb-global-contentbox-tpl_jzvd0f89" data-css="tve-u-16d824e7536">
<div class="tve-content-box-background tcb-global-contentbox-tpl_jzvd0f89-bg" data-css="tve-u-16d824e754a"></div>
<div class="tve-cb tcb-global-contentbox-tpl_jzvd0f89-cb" data-css="tve-u-16d824e7569"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top dynamic-group-jzwirpjx" data-css="tve-u-168802db583" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870238647"></div>
<div class="tve-cb" data-css="tve-u-1687b292de9"></div>
</div><div class="thrv_wrapper tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top thrv_text_element dynamic-group-jzwise3g" data-tag="h5" data-css="tve-u-1687b27fa68" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__"><h5 data-css="tve-u-16904854168" style="text-align: center;" class=""><strong>Bonus 1</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiswby" data-tag="h6" data-css="tve-u-1687b28b46a"><h6 style="text-align: center;" class=""><strong>Title of your first bonus</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiu1m7" data-css="tve-u-16870271031" data-float="1"><p data-css="tve-u-16870278f9b" style="text-align: center;">Tell them all about this <em><span data-css="tve-u-1687a1f3ec0" style="color: rgb(8, 8, 18);">other</span></em> great thing that they also get. Maybe it’s access to a members-only facebook group or online forum. If so, tell them about it here!</p><p data-css="tve-u-1687027a265" style="text-align: center;"><br></p><p data-css="tve-u-1687027a268" style="text-align: center;">Tell them why this bonus feature of your course is going to help them achieve <em><span data-css="tve-u-1687a1f623e" style="color: rgb(8, 8, 18);">even more</span></em> than what you’ve already outlined in the modules above. 1-on-1 coaching calls, downloadable workbooks, cheat sheets… whatever it is, mention it in this section.</p></div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwiyfdj"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiuuku tcb-global-contentbox-tpl_jzvd0f89" data-css="tve-u-16d824e753b">
<div class="tve-content-box-background tcb-global-contentbox-tpl_jzvd0f89-bg" data-css="tve-u-16d824e754c"></div>
<div class="tve-cb tcb-global-contentbox-tpl_jzvd0f89-cb" data-css="tve-u-16d824e756b"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top dynamic-group-jzwirpjx" data-css="tve-u-16afd3ab1de" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<div class="tve-content-box-background" data-css="tve-u-16cd88efb39"></div>
<div class="tve-cb" data-css="tve-u-1687b292de9"></div>
</div><div class="thrv_wrapper tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top thrv_text_element dynamic-group-jzwise3g" data-tag="h5" data-css="tve-u-1687b2941b3" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__"><h5 data-css="tve-u-16904855801" style="text-align: center;" class=""><strong>Bonus 2</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiswby" data-tag="h6" data-css="tve-u-1687b294dfc"><h6 style="text-align: center;" class=""><strong>Title of your second bonus</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiu1m7" data-css="tve-u-16861104c11"><p data-css="tve-u-16870278f9b" style="text-align: center;">Tell them all about this&nbsp;<em><span data-css="tve-u-1687a1f3ec0">other</span></em> great thing that they also get. Maybe it’s access to a members-only facebook group or online forum. If so, tell them about it here!</p><p data-css="tve-u-1687027a265" style=""><br></p><p data-css="tve-u-1687027a268" style="text-align: center;">Tell them why this bonus feature of your course is going to help them achieve&nbsp;<em><span data-css="tve-u-1687a1f623e">even more</span></em> than what you’ve already outlined in the modules above. 1-on-1 coaching calls, downloadable workbooks, cheat sheets… whatever it is, mention it in this section.</p></div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwiyfdj"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwiuuku tcb-global-contentbox-tpl_jzvd0f89" data-css="tve-u-16d824e753d">
<div class="tve-content-box-background tcb-global-contentbox-tpl_jzvd0f89-bg" data-css="tve-u-16d824e754d"></div>
<div class="tve-cb tcb-global-contentbox-tpl_jzvd0f89-cb" data-css="tve-u-16d824e756d"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top dynamic-group-jzwirpjx" data-css="tve-u-16afd39b9e5" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<div class="tve-content-box-background" data-css="tve-u-16cd8906ea9"></div>
<div class="tve-cb" data-css="tve-u-1687b292de9"></div>
</div><div class="thrv_wrapper tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_slide_top thrv_text_element dynamic-group-jzwise3g" data-tag="h5" data-css="tve-u-1686ff61421" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;slide_top&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__"><h5 data-css="tve-u-16904856f93" style="text-align: center;" class=""><strong>Bonus 3</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiswby" data-tag="h6" data-css="tve-u-1686ffdf19b"><h6 style="text-align: center;" class=""><strong>Title of your third bonus</strong></h6></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwiu1m7" data-css="tve-u-16861104c11"><p data-css="tve-u-16870278f9b" style="text-align: center;">Tell them all about this <em><span data-css="tve-u-1687a1f3ec0">other</span></em>&nbsp;great thing that they also get. Maybe it’s access to a members-only facebook group or online forum. If so, tell them about it here!</p><p data-css="tve-u-1687027a265" style="text-align: center;"><br></p><p data-css="tve-u-1687027a268" style="text-align: center;">Tell them why this bonus feature of your course is going to help them achieve&nbsp;<em><span data-css="tve-u-1687a1f623e">even more</span></em> than what you’ve already outlined in the modules above. 1-on-1 coaching calls, downloadable workbooks, cheat sheets… whatever it is, mention it in this section.</p></div></div>
</div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168702a30e1">
<div class="tve-page-section-out" data-css="tve-u-1687028504c"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1000 78" preserveAspectRatio="none" decoration-type="waves" class="svg-shape-bottom" data-css="tve-u-1687030c2cc"><path d="M0,0v60c51.1-27,123.1-42,216-45c122-4,207.4,27.3,443,38c72.6,3.3,186.3-4.4,341-23V0H0z"></path><path opacity="0.6" d="M1,1v60c23.1-17,81.1-27,174-30c122-4,169.4,32.3,405,43c72.6,3.3,213-11,421-43V1H1z"></path><path opacity="0.2" d="M1,0v62c17.8-9,73.1-15,166-18c122-4,188,18,366,18c62,0,147.7-9,314-9     c32.1,0,83.4,6,154,18V0H1z"></path></svg></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-16870296ab2"><div class="thrv_wrapper thrv-columns" data-css="tve-u-1687029032a"><div class="tcb-flex-row tcb-resized tcb--cols--3 v-2 tcb-medium-wrap tcb-desktop-no-wrap tcb-row-reversed-mobile" data-css="tve-u-168702a6599"><div class="tcb-flex-col" data-css="tve-u-1687028b7d6" style=""><div class="tcb-col" data-css="tve-u-168702c2c81"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168702a4e47" data-tag="h2"><h2 class="" data-css="tve-u-168702be67e" style="">About The Course Teacher,<br><strong>(Name Here)</strong></h2></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168703e170a" data-float="1"><p data-css="tve-u-1687029cf00" style="text-align: left;">Write this section in third-person, meaning you should not use the words ‘I’ or ‘We’ or ‘Me’. Instead, talk about the course teacher objectively using 'he' or 'she' even if you are the teacher! It’s time to show off your achievements and prove why you are the right person to be teaching this topic.</p><p data-css="tve-u-1687029cf00" style="text-align: left;"><br></p><p data-css="tve-u-1687029cf00" style="text-align: left;">But just after you’ve talked yourself up, make sure you end on a friendly note. You don’t want to sound intimidating, you want to sound knowledgeable and friendly.</p></div></div></div><div class="tcb-flex-col" data-css="tve-u-16870413e55" style=""><div class="tcb-col"><div class="thrv_wrapper tve_image_caption tcb-mobile-hidden" data-css="tve-u-16870414d56" style=""><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31735" alt="" width="438" height="656" title="Online_Course_authorimg2" data-id="31735" src="{tcb_lp_base_url}/css/images/Online_Course_authorimg2.jpg" style=""></span></div></div></div><div class="tcb-flex-col" data-css="tve-u-1687028b836" style=""><div class="tcb-col" data-css="tve-u-16870b6bfff"><div class="thrv_wrapper tve_image_caption" data-css="tve-u-16870c47d53"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31936" alt="" width="623" height="765" title="Online_Course_about" data-id="31936" src="{tcb_lp_base_url}/css/images/Online_Course_about.jpg" style=""></span></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzvc9xyj" data-css="tve-u-16cd892b6a4">
<div class="tve-page-section-out tcb-global-section-tpl_jzvc9xyj-out" data-css="tve-u-16cd892b6c1"></div>
<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzvc9xyj-in" data-css="tve-u-16cd892b6b4"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-1687a2e3a06"><h3 class="" style="text-align: center;"><!--StartFragment-->Here’s what people are saying about the course instructor<!--EndFragment--></h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687b4eaadd"><p data-css="tve-u-16afd52a64f" style="text-align: center;">These 3 testimonials are just after you’ve introduced the teacher/ instructor, so they should focus on&nbsp;<em>how great the course teacher is and why they are worth trusting.</em> If you haven’t got great testimonials yet, then check out our article on the <a href="https://thrivethemes.com/get-raving-testimonials-for-your-online-course/" target="_blank">secret to gettin​​g raving testimonials for your online course</a></p></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" style="" data-css="tve-u-1602b1ebd26" data-ct-name="Resume Clients" data-ct="testimonial-7342" data-element-name="Testimonial">
<div class="tve-content-box-background"></div>
<div class="tve-cb tve_empty_dropzone" data-css="tve-u-16cdce7979b"><div class="thrv_wrapper thrv-columns" data-css="tve-u-1687a64596d"><div class="tcb-flex-row tcb-row-reversed-tablet tcb-row-reversed-mobile tcb-desktop-no-wrap v-2 tcb--cols--3" data-css="tve-u-168605473c1"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwjopn3" data-css="tve-u-168605c3908"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjo5fj" data-css="tve-u-168803954fa">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmr2f" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjluh3" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwjlki9" data-css="tve-u-16cdcc8cf6b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31895" alt="" width="94" height="94" title="Online_Course_person4" data-id="31895" src="{tcb_lp_base_url}/css/images/Online_Course_person4.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmdiw" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjkv2n" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd865fa80" style=""><strong><strong>Christian Doe</strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjlaqv" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Architect</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjnut3" data-css="tve-u-16880375cd8">
<div class="tve-content-box-background" data-css="tve-u-16870a05543" data-clip-id="28e3b0536e661"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd41f18c"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnae7" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnkvb" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials are just after you’ve introduced the teacher/ instructor, so they should focus on <em>how great the course teacher is and why they are worth trusting.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col" data-css="tve-u-1687b466ee7"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjo5fj" data-css="tve-u-1688039a182">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmr2f" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjluh3" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwjlki9" data-css="tve-u-16cdcc8cf6b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31897" alt="" width="128" height="128" title="Online_Course_person5" data-id="31897" src="{tcb_lp_base_url}/css/images/Online_Course_person5.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmdiw" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjkv2n" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd8661251" style=""><strong><strong><strong>Barbara Lane</strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjlaqv" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Art Director</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjnut3" data-css="tve-u-168803a5e31">
<div class="tve-content-box-background" data-css="tve-u-16870a05543" data-clip-id="d882e4893e369"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd4201aa"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnae7" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnkvb" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials are just after you’ve introduced the teacher/ instructor, so they should focus on <em>how great the course teacher is and why they are worth trusting.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwjopn3" data-css="tve-u-168605c5931"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjo5fj" data-css="tve-u-1687b4e1cc5">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmr2f" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjluh3" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwjlki9" data-css="tve-u-16cdcc8cf6b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-31899" alt="" width="128" height="128" title="Online_Course_person6" data-id="31899" src="{tcb_lp_base_url}/css/images/Online_Course_person6.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwjmdiw" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjkv2n" data-css="tve-u-16870e1017e"><p data-css="tve-u-16cd866a849" style=""><strong><strong><strong><strong>Marie Jones</strong></strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjlaqv" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Marketing Specialist</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwjnut3" data-css="tve-u-168803a7cd8">
<div class="tve-content-box-background" data-css="tve-u-16870a05543" data-clip-id="ed1a4da63cab8"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16afd421e5b"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnae7" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwjnkvb" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials are just after you’ve introduced the teacher/ instructor, so they should focus on <em>how great the course teacher is and why they are worth trusting.</em><!--EndFragment--></p></div></div>
</div></div>
</div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-16870e466c1">
<div class="tve-page-section-out" data-css="tve-u-1687114df6a" data-clip-id="cac3eea6ec323"><svg width="0" height="0" class="tve-decoration-svg"><defs><clipPath id="clip-bottom-cac3eea6ec323" class="decoration-clip clip-path-bottom" clipPathUnits="objectBoundingBox" data-screen="" decoration-type="divider" pointer-width="100" pointer-height="12" style="curves"><polygon points="0 0, 0 0.88, 1 0.88, 1 0"></polygon><ellipse cx="0.5" cy="0.88" rx="0.500000" ry="0.12"></ellipse></clipPath><clipPath id="clip-tablet-bottom-cac3eea6ec323" class="decoration-clip clip-path-tablet-bottom" clipPathUnits="objectBoundingBox" data-screen="tablet-" decoration-type="divider" pointer-width="100" pointer-height="5" style="curves"><polygon points="0 0, 0 0.95, 1 0.95, 1 0"></polygon><ellipse cx="0.5" cy="0.95" rx="0.500000" ry="0.05"></ellipse></clipPath><clipPath id="clip-mobile-bottom-cac3eea6ec323" class="decoration-clip clip-path-mobile-bottom" clipPathUnits="objectBoundingBox" data-screen="mobile-" decoration-type="divider" pointer-width="100" pointer-height="3" style="curves"><polygon points="0 0, 0 0.97, 1 0.97, 1 0"></polygon><ellipse cx="0.5" cy="0.97" rx="0.500000" ry="0.03"></ellipse></clipPath></defs></svg></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-16870e446bf"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-16860dbc5e4"><h3 style="text-align: center;" class=""><!--StartFragment--><!--StartFragment-->Subheading About Why Your Course Is Valuable<!--EndFragment-->
<!--EndFragment--></h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-16870f5632c"><p data-css="tve-u-168709e7e48" style="text-align: center;">This section is all about <strong>price anchoring</strong>. In a moment, your visitor will be seeing the price of your online course, so you want to begin talking about the value of your course in this section so that when they finally see the price, they feel that it is reasonable.</p><p data-css="tve-u-168709e7e48" style="text-align: center;">This section is not required, but it is common on Sales Pages for online courses, especially if they are highly priced.</p><p data-css="tve-u-168709e7e48" style="text-align: center;">If you do want to include this section, help the visitor by explaining how the alternate solutions to their problems can cost much, much more than your course.</p><p data-css="tve-u-16afd698d2e" style="text-align: center;">&nbsp;Perhaps you want to tell them what it costs to get this equivalent teaching at university. Or perhaps you’ll tell them how much you would charge for a few hours of private coaching. The goal is to give them something to gauge the upcoming price against.<br><br>This section works best if you directly list monetary numbers, such as $5000. Anyone skimming the page will immediately stop when they see a dollar sign and read the text around it. When they realise this is not the course price, they’ll keep scrolling until they do find the price and gauge it against the first number they saw.</p></div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzvc9xyj" data-css="tve-u-16cd892d44f">
<div class="tve-page-section-out tcb-global-section-tpl_jzvc9xyj-out" data-clip-id="26fcf676b5c7a" data-css="tve-u-16cd892d47f"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzvc9xyj-in" data-css="tve-u-16cd892d45d"><div class="thrv_wrapper thrv-pricing-table" data-ct="pricing_table-25235" data-ct-name="I can&amp;#8217;t believe it&amp;#8217;s not PS 3-Column with toggle" data-css="tve-u-1687113dbe8" id="Pricing">
<div class="thrv_wrapper thrv-button-group tcb-no-clone tcb-no-delete tve_no_drag tcb-no-save" data-price-button-group="tve-u-price-button-group-164d1086276" data-css="tve-u-1687f866476"><div class="thrv_wrapper thrv-button-group-item tcb-no-clone tcb-no-delete tve_no_drag tcb-no-title tcb-no-save tcb-active-state" data-instance="1762587" data-selector="[data-price-button-group=&quot;tve-u-price-button-group-164d1086276&quot;] .thrv-button-group-item.tcb-active-state" data-default="true">
<a href="#" class="tcb-button-link">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-1687f819282">One Time Payment</span></span>
</a>
</div><div class="thrv_wrapper thrv-button-group-item tcb-no-clone tcb-no-delete tve_no_drag tcb-no-title tcb-no-save" data-instance="93776" data-selector="[data-price-button-group=&quot;tve-u-price-button-group-164d1086276&quot;] .thrv-button-group-item:not(.tcb-active-state)" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text"><strong>3 m​onth instalments</strong></span></span>
</a>
</div></div>
<div class="tcb-flex-row tcb-pricing-table-box-container tcb--cols--2" data-instance="1762587" style="" data-css="tve-u-1687f815778"><div class="tcb-flex-col" data-label="Regular">
<div class="tcb-col dynamic-group-jzwlgcfd">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwl8slr" data-css="tve-u-1687154e0a3">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-1687120859b">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl8i79" data-css="tve-u-1687f7bb943">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f7bfc52"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwl37p0" data-css="tve-u-1687f7c429b" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f7bfc54"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl2lsa" data-css="tve-u-16cdd1d6041"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-light" data-name="">
<path d="M356 160H188c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12zm12 52v-8c0-6.6-5.4-12-12-12H188c-6.6 0-12 5.4-12 12v8c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12zm64.7 268h3.3c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H80c-44.2 0-80-35.8-80-80V80C0 35.8 35.8 0 80 0h344c13.3 0 24 10.7 24 24v368c0 10-6.2 18.6-14.9 22.2-3.6 16.1-4.4 45.6-.4 65.8zM128 384h288V32H128v352zm-96 16c13.4-10 30-16 48-16h16V32H80c-26.5 0-48 21.5-48 48v320zm372.3 80c-3.1-20.4-2.9-45.2 0-64H80c-64 0-64 64 0 64h324.3z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl45el" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f84a601" style=""><strong>regular course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl8252" data-tag="p" data-css="tve-u-168715544f9"><p class="tcb-pt-card-title" data-css="tve-u-16afda1a88b" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8b9223">
<div class="tve-content-box-background" data-css="tve-u-16cd86513b2"></div>
<div class="tve-cb" data-css="tve-u-16afd71f91e"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwl3vj0" data-css="tve-u-16afd729923"><p data-css="tve-u-16afd728298" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwl3jno" data-css="tve-u-16870f982b2"><p data-css="tve-u-1687f79fe5c" style="">247</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl7qja" data-css="tve-u-168803f9277">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16cdd442bc6"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-16880410cf7">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
</ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs dynamic-group-jzwl4lwe" style="" data-tcb_hover_state_parent="" data-css="tve-u-16cd87a29a7" data-button-style="tcb-global-button-tpl_jzvb5fzs">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext">Enroll Now</span></span>
</a>
</div>
</div>
</div>
</div>
</div><div class="tcb-flex-col" data-label="Advanced">
<div class="tcb-col dynamic-group-jzwlgcfd">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwl8slr" data-css="tve-u-1687117842d">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-16870fac08c">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl8i79" data-css="tve-u-1687f7bcd9c">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwl37p0" data-css="tve-u-1687f7d87cc" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f7da7a7"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl2lsa" data-css="tve-u-16cdd1d6b26"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-heart-light" data-name="">
<path d="M230.1 307.9c5.5 5.5 14.3 5.5 19.8 0l84.7-85.6c24.6-24.9 23.2-66.1-4.3-89.1-13.9-11.6-47.7-28.5-90.2 14.5-42.6-43-76.4-26.1-90.2-14.5-27.5 23-28.9 64.2-4.3 89.1l84.5 85.6zm-59.8-150.1c13.7-11.5 31.2-3.4 38.4 3.7l31.4 31.7 31.4-31.7c7.1-7.2 24.6-15.2 38.4-3.7 14.4 12 12.3 31.6 2.1 42l-72 72.6-71.8-72.6c-10.3-10.4-12.3-30 2.1-42zM448 392V24c0-13.3-10.7-24-24-24H80C35.8 0 0 35.8 0 80v352c0 44.2 35.8 80 80 80h356c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12h-3.3c-4-20.2-3.2-49.7.4-65.8 8.7-3.6 14.9-12.2 14.9-22.2zm-43.7 88H80c-64 0-64-64 0-64h324.3c-2.9 18.8-3.1 43.6 0 64zm11.7-96H80c-18 0-34.6 6-48 16V80c0-26.5 21.5-48 48-48h336v352z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl45el" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f84b879" style=""><strong>Advanced course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl8252" data-tag="p" data-css="tve-u-16871554d8a"><p class="tcb-pt-card-title" data-css="tve-u-16afda1bffc" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8ba79b">
<div class="tve-content-box-background" data-css="tve-u-16cd85ce7b3"></div>
<div class="tve-cb" data-css="tve-u-16afd721aa0"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwl3vj0" data-css="tve-u-16afd72a869"><p data-css="tve-u-16afd7ba114" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwl3jno" data-css="tve-u-16870f982b2"><p data-css="tve-u-1687f7a10bc" style="">347</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl7qja" data-css="tve-u-1687f83b5f1">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16cdd442bd1"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-1687f83cf2c">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d0cb6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">X number of bonuses</span></li><li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d14b5"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to Private Facebook Group</span></li><li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d16a6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">30 minute private coaching call</span></li></ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs dynamic-group-jzwl4lwe" style="" data-tcb_hover_state_parent="" data-css="tve-u-16cd87a3e8f" data-button-style="tcb-global-button-tpl_jzvb5fzs">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext">Enroll Now</span></span>
</a>
</div>
</div>
</div></div>
</div></div>
<div class="tcb-flex-row tcb-pricing-table-box-container tcb--cols--2 tcb-permanently-hidden" data-instance="93776" style="" data-css="tve-u-1687f8a5583"><div class="tcb-flex-col" data-label="Regular Course">
<div class="tcb-col dynamic-group-jzwlfr20">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwlfckf" data-css="tve-u-1687102d21e">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-1687120859b">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlbfyu" data-css="tve-u-1687f8766a4">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16afd6cefc7"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwla73x" data-css="tve-u-1687f878881" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f87a4f0"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl9myh" data-css="tve-u-16afd69334d"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-light" data-name="">
<path d="M356 160H188c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12zm12 52v-8c0-6.6-5.4-12-12-12H188c-6.6 0-12 5.4-12 12v8c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12zm64.7 268h3.3c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H80c-44.2 0-80-35.8-80-80V80C0 35.8 35.8 0 80 0h344c13.3 0 24 10.7 24 24v368c0 10-6.2 18.6-14.9 22.2-3.6 16.1-4.4 45.6-.4 65.8zM128 384h288V32H128v352zm-96 16c13.4-10 30-16 48-16h16V32H80c-26.5 0-48 21.5-48 48v320zm372.3 80c-3.1-20.4-2.9-45.2 0-64H80c-64 0-64 64 0 64h324.3z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlast5" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f8a1e21" style=""><strong>regular course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlb6q1" data-tag="p" data-css="tve-u-1687136478c"><p class="tcb-pt-card-title" data-css="tve-u-16afda1feff" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8b0b71">
<div class="tve-content-box-background" data-css="tve-u-1687101c151"></div>
<div class="tve-cb" data-css="tve-u-16afd6d290f"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlch73" data-css="tve-u-1687f8989d0"><p data-css="tve-u-16afd6fe129" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwlbugq" data-css="tve-u-16afd70189b"><p data-css="tve-u-1687f891462" style="">99</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlcuzb" data-css="tve-u-1687143ac9b"><p data-css="tve-u-1687f893db3" style="">/month</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlf27m" data-css="tve-u-1688041ecc9">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16cdd4491a8"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-16871034e8b">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
</ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right dynamic-group-jzwleqrz" data-css="tve-u-168712ff61d" style="" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-16afd76e217">Enroll Now</span></span>
</a>
</div>
</div>
</div>
</div>
</div><div class="tcb-flex-col" data-label="Advanced Course">
<div class="tcb-col dynamic-group-jzwlfr20">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwlfckf" data-css="tve-u-1687117842d">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-16870fac08c">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlbfyu" data-css="tve-u-1687f8774bd">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16afd6d00dd"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwla73x" data-css="tve-u-1687f887406" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f887d89"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl9myh" data-css="tve-u-16afd4756b1"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-heart-light" data-name="">
<path d="M230.1 307.9c5.5 5.5 14.3 5.5 19.8 0l84.7-85.6c24.6-24.9 23.2-66.1-4.3-89.1-13.9-11.6-47.7-28.5-90.2 14.5-42.6-43-76.4-26.1-90.2-14.5-27.5 23-28.9 64.2-4.3 89.1l84.5 85.6zm-59.8-150.1c13.7-11.5 31.2-3.4 38.4 3.7l31.4 31.7 31.4-31.7c7.1-7.2 24.6-15.2 38.4-3.7 14.4 12 12.3 31.6 2.1 42l-72 72.6-71.8-72.6c-10.3-10.4-12.3-30 2.1-42zM448 392V24c0-13.3-10.7-24-24-24H80C35.8 0 0 35.8 0 80v352c0 44.2 35.8 80 80 80h356c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12h-3.3c-4-20.2-3.2-49.7.4-65.8 8.7-3.6 14.9-12.2 14.9-22.2zm-43.7 88H80c-64 0-64-64 0-64h324.3c-2.9 18.8-3.1 43.6 0 64zm11.7-96H80c-18 0-34.6 6-48 16V80c0-26.5 21.5-48 48-48h336v352z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlast5" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f8a2b5b" style=""><strong>Advanced course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlb6q1" data-tag="p" data-css="tve-u-168713653ad"><p class="tcb-pt-card-title" data-css="tve-u-16afda1e9e5" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8af4d6">
<div class="tve-content-box-background" data-css="tve-u-16871188546"></div>
<div class="tve-cb" data-css="tve-u-16afd6d3cd1"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlch73" data-css="tve-u-1687f89a536"><p data-css="tve-u-16afd70405b" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwlbugq" data-css="tve-u-16afd70c8e5"><p data-css="tve-u-1687f892200" style="">127</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlcuzb" data-css="tve-u-1687143ac9b"><p data-css="tve-u-1687f895b34" style="">/month</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlf27m" data-css="tve-u-168804269a6">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16cdd4491b3"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-168804251a7">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d0cb6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">X number of bonuses</span></li><li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d14b5"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to Private Facebook Group</span></li><li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d16a6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">30 minute private coaching call</span></li></ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right dynamic-group-jzwleqrz" data-css="tve-u-168712ff621" style="" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-16afd76d41d">Enroll Now</span></span>
</a>
</div>
</div>
</div></div>
</div></div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1685bfe6ef0">
<div class="tve-content-box-background" data-css="tve-u-1685bfeadf6"></div>
<div class="tve-cb" data-css="tve-u-16afd27ad3a"><div class="thrv_wrapper thrv-columns" data-css="tve-u-16afdb05f30"><div class="tcb-flex-row tcb--cols--2 tcb-resized v-2 tcb-medium-wrap" data-css="tve-u-1685bff71f4"><div class="tcb-flex-col" data-css="tve-u-1687a73be33" style=""><div class="tcb-col" data-css="tve-u-16cdd3e74e3"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687fb2d2dd" data-tag="h4"><h4 class="" style="">Course Enrollments close on&nbsp;<strong>July 25th 2019</strong></h4></div></div></div><div class="tcb-flex-col" data-css="tve-u-1687a73be38" style=""><div class="tcb-col" data-css="tve-u-16cd8ba83c3"><div class="tcb-clear" data-css="tve-u-16afdb63082"><div class="thrv_wrapper thrv_countdown_timer thrv-countdown_timer_plain tve_clearfix tve_red tve_countdown_2" data-date="2019-10-25" data-hour="22" data-min="14" data-timezone="+00:00" data-css="tve-u-16afdb75009">
<div class="sc_timer_content tve_clearfix tve_block_center">
<div class="tve_t_day tve_t_part">
<div class="t-digits" style=""><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8b6e832">Days</div>
</div>
<div class="tve_t_hour tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption">Hours</div>
</div>
<div class="tve_t_min tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8b7135e">Minutes</div>
</div>
<div class="tve_t_sec tve_t_part">
<div class="t-digits"><span class="part-2">0</span><span class="part-1">0</span></div>
<div class="thrv-inline-text t-caption" data-css="tve-u-16cd8b7298f">Seconds</div>
</div>
<div class="tve_t_text" style="display: none;"></div>
</div>
</div></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-168753d29c9" data-inherit-lp-settings="1">
<div class="tve-page-section-out" data-css="tve-u-168753bdcc6"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-168753a92d6"><div class="thrv_wrapper thrv-columns" data-css="tve-u-168755e8288"><div class="tcb-flex-row tcb--cols--2 v-2 m-edit tcb-resized tcb-medium-wrap tcb-mobile-wrap" data-css="tve-u-1687541ed6d"><div class="tcb-flex-col c-33" data-css="tve-u-1687f908b74" style=""><div class="tcb-col"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-168753f32f4">
<div class="tve-content-box-background" data-css="tve-u-168753f4752"></div>
<div class="tve-cb" data-css="tve-u-168753f7ad7"><div class="thrv_wrapper tve_image_caption" data-css="tve-u-168753dc9fb" style=""><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-5718" alt="" width="150" height="150" title="guarantee-white" data-id="5718" src="{tcb_lp_base_url}/css/images/guarantee-white-150x150.png" style=""></span></div></div>
</div></div></div><div class="tcb-flex-col c-66" data-css="tve-u-1687f908c29" style=""><div class="tcb-col" data-css="tve-u-16cdd3eb6dc"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168754d43e5" data-tag="h2"><h2 class="" data-css="tve-u-1688068aa8c" style="">100% Satisfaction Guarantee for <strong>30-Days</strong></h2></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-168755c3de5"><p data-css="tve-u-168754ec6b1" style="">It has been proven time and time again that offering a satisfaction guarantee or refund policy increases sales. And not just by a little bit. It makes a noticeable difference to conversions.</p><p data-css="tve-u-168754ec6b1" style=""><br></p><p data-css="tve-u-168754ec6b1" style="">This section is important. Right after you’ve introduced the price of your online course, you need to do everything you can to alleviate purchase anxiety. As soon as a visitor sees the price, they will start to think of all the reasons why they <em>shouldn’t</em> buy. That’s why it’s important to have your refund policy immediately after the first time your price is mentioned.</p></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-1687578c545">
<div class="tve-page-section-out" data-css="tve-u-168757a028a" data-clip-id="01795ce3f8788"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1687a62ea19"><div class="thrv_wrapper thrv_text_element" data-tag="h3" data-css="tve-u-1687a342623"><h3 class="" style="text-align: center;"><!--StartFragment-->Don’t wait! Look at what past students have to say about this course<!--EndFragment--></h3></div><div class="thrv_wrapper thrv_text_element tve-froala fr-box" data-css="tve-u-1686056037c"><p data-css="tve-u-16afd7ca9f1" style="text-align: center;">These 3 testimonials should focus on&nbsp;<em>why past customers found that your course was absolutely the right decision</em>. Remember, your visitors are looking for reasons&nbsp;<em>not</em> to buy after they see the price, so include some testimonials here that alleviate that anxiety. If you haven’t got great testimonials yet, then check out our article on the <a class="tve-froala" href="https://thrivethemes.com/get-raving-testimonials-for-your-online-course/" style="outline: none; display: inline-block;" target="_blank">secret to getting raving testimonials for your online course</a></p></div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" style="" data-css="tve-u-1602b1ebd26" data-ct-name="Resume Clients" data-ct="testimonial-7342" data-element-name="Testimonial">
<div class="tve-content-box-background"></div>
<div class="tve-cb tve_empty_dropzone" data-css="tve-u-16cdd1ec2b5"><div class="thrv_wrapper thrv-columns" data-css="tve-u-1687a632de7"><div class="tcb-flex-row tcb-row-reversed-tablet tcb-row-reversed-mobile tcb-desktop-no-wrap v-2 tcb--cols--3" data-css="tve-u-168605473c1"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwlrtym" data-css="tve-u-168605c3908"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlrirz" data-css="tve-u-1688047d381">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlq5x7" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlpi5g" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwlp8nf" data-css="tve-u-16cdcca103b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-32248" alt="" width="128" height="128" title="Online_Course_person7" data-id="32248" src="{tcb_lp_base_url}/css/images/Online_Course_person7.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlptzv" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlolva" data-css="tve-u-16870e1017e"><p data-css="tve-u-168806b3247" style=""><strong>Debra Grant</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwloz7n" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Reporter</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlr88z" data-css="tve-u-168804789ce">
<div class="tve-content-box-background" data-css="tve-u-168757a2bdb" data-clip-id="6cf8714d17278"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-1687a46d80c"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqlos" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqym5" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials should focus on <em>why past customers found that your course was absolutely the right decision</em>. Remember, your visitors are looking for reasons <em>not</em> to buy after they see the price, so include some testimonials here that alleviate that anxiety.<!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col" data-css="tve-u-168605c47f7"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlrirz" data-css="tve-u-1687f96d500">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlq5x7" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlpi5g" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwlp8nf" data-css="tve-u-16cdcca103b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-32309" alt="" width="94" height="94" title="Online_Course_person8" data-id="32309" src="{tcb_lp_base_url}/css/images/Online_Course_person8-1.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlptzv" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlolva" data-css="tve-u-16870e1017e"><p data-css="tve-u-168806b3247" style=""><strong><strong>Linda Parker</strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwloz7n" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Police Officer</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlr88z" data-css="tve-u-16880484dc3">
<div class="tve-content-box-background" data-css="tve-u-168757a89bd" data-clip-id="91996e43afec9"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16870a0246d"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqlos" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqym5" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials should focus on <em>why past customers found that your course was absolutely the right decision</em>. Remember, your visitors are looking for reasons <em>not</em> to buy after they see the price, so include some testimonials here that alleviate that anxiety.<!--EndFragment--></p></div></div>
</div></div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwlrtym" data-css="tve-u-168605c5931"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlrirz" data-css="tve-u-1687091b07a">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlq5x7" data-css="tve-u-16afdd5028d">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="tcb-clear" data-css="tve-u-16afdd58015"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlpi5g" data-css="tve-u-16afdd0b596" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-16870a10c3e"></div>
<div class="tve-cb" data-css="tve-u-1686061b5f0"><div class="thrv_wrapper tve_image_caption dynamic-group-jzwlp8nf" data-css="tve-u-16cdcca103b"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-32252" alt="" width="96" height="96" title="Online_Course_person9" data-id="32252" src="{tcb_lp_base_url}/css/images/Online_Course_person9.jpg" style=""></span></div></div>
</div></div><div class="tcb-clear" data-css="tve-u-16afdd660ab"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve-elem-default-pad dynamic-group-jzwlptzv" data-css="tve-u-16afdd660a7" data-float="1">
	<div class="tve-content-box-background"></div>
	<div class="tve-cb"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlolva" data-css="tve-u-16870e1017e"><p data-css="tve-u-168806b3247" style=""><strong><strong><strong>Glenda Sanders</strong></strong></strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwloz7n" data-css="tve-u-16afdd60367"><p data-css="tve-u-16afd7cd38f" style="">Financial Advisor</p></div></div>
</div></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlr88z" data-css="tve-u-1686057430e">
<div class="tve-content-box-background" data-css="tve-u-168757ad93f" data-clip-id="970d769094d57"><svg width="0" height="0" class="tve-decoration-svg"><defs></defs></svg></div>
<div class="tve-cb" data-css="tve-u-16870a0246d"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqlos" data-css="tve-u-1686101d6db"><p data-css="tve-u-168605f9847" style="text-align: left;"><strong>Pull out a few key words for a testimonial title</strong></p></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlqym5" data-css="tve-u-168605f888e"><p data-css="tve-u-1686119f5bd" style=""><!--StartFragment-->These 3 testimonials should focus on <em>why past customers found that your course was absolutely the right decision</em>. Remember, your visitors are looking for reasons <em>not</em> to buy after they see the price, so include some testimonials here that alleviate that anxiety.<!--EndFragment--></p></div></div>
</div></div>
</div></div></div></div></div></div>
</div></div>
</div><div class="thrv_wrapper thrv-page-section tcb-global-section-tpl_jzwne0o8" data-inherit-lp-settings="1" data-css="tve-u-16cdd4acc71">
	<div class="tve-page-section-out tcb-global-section-tpl_jzwne0o8-out" data-css="tve-u-16cdd4acc8e"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1000 78" preserveAspectRatio="none" decoration-type="waves" class="svg-shape-top" data-css="tve-u-16898dd3bd0"><path d="M0,0v60c51.1-27,123.1-42,216-45c122-4,207.4,27.3,443,38c72.6,3.3,186.3-4.4,341-23V0H0z"></path><path opacity="0.6" d="M1,1v60c23.1-17,81.1-27,174-30c122-4,169.4,32.3,405,43c72.6,3.3,213-11,421-43V1H1z"></path><path opacity="0.2" d="M1,0v62c17.8-9,73.1-15,166-18c122-4,188,18,366,18c62,0,147.7-9,314-9     c32.1,0,83.4,6,154,18V0H1z"></path></svg></div>
	<div class="tve-page-section-in tve_empty_dropzone tcb-global-section-tpl_jzwne0o8-in" data-css="tve-u-16cdd4acc80"><div class="thrv_wrapper thrv_text_element" data-tag="h2" data-css="tve-u-16899a6ae75"><h2 style="text-align: center;" class=""><!--StartFragment--><!--StartFragment-->Frequently Asked Questions<!--EndFragment-->
<!--EndFragment--></h2></div><div class="thrv_wrapper thrv-columns dynamic-group-jzwlvtwd" data-css="tve-u-16898db49ac"><div class="tcb-flex-row v-2 tcb--cols--2" data-css="tve-u-16898db468a"><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwlvkil" data-css="tve-u-16899be2e7e"><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899be7319" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899bd402a" style=""><!--StartFragment--><!--StartFragment--><strong>What are common questions abou​​​​t your course?​</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899be4c12" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899bd673c" style=""><!--StartFragment--><!--StartFragment--><strong><!--StartFragment-->Can You Put Example Questions Here?<!--EndFragment-->&nbsp;</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899a9d9c3" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899b2035f" style=""><!--StartFragment--><!--StartFragment--><strong><!--StartFragment-->Can You Put Example Questions Here?<!--EndFragment-->&nbsp;</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div></div></div><div class="tcb-flex-col"><div class="tcb-col dynamic-group-jzwlvkil"><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899bf19eb" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899bd4f6a" style=""><!--StartFragment--><!--StartFragment--><strong>What are common questions abou​​​​t your course?​</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899bef9ab" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899bd7ef4" style=""><!--StartFragment--><!--StartFragment--><strong><!--StartFragment-->Can You Put Example Questions Here?<!--EndFragment-->&nbsp;</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div><div class="thrv_wrapper thrv_toggle_shortcode dynamic-group-jzwlt8v0" data-hover-color="var(--tcb-color-1)" data-css="tve-u-16899aa48d6" data-tcb_hover_state_parent="" data-text-hover-color="var(--tcb-color-14)">
	<div class="tve_faq">
		<div class="tve_faqI">
			<div class="tve_faqB"><span class="tve_not_editable tve_toggle"></span>
				<h4 class="tve_editable dynamic-group-jzwlskyu" data-css="tve-u-16899b300c7" style=""><!--StartFragment--><!--StartFragment--><strong><!--StartFragment-->Can You Put Example Questions Here?<!--EndFragment-->&nbsp;</strong><!--EndFragment-->
<!--EndFragment--></h4>
			</div>
			<div class="tve_faqC" style="display: none;"><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlsytw" data-css="tve-u-16898bad441"><p data-css="tve-u-16899a31bb5" style=""><!--StartFragment-->Provide some answers to common questions. Perhaps you want to tell answer how much time they need to dedicate to get through the course, or give them a support email to contact you if they have further questions. It doesn’t need to be questions you were actually asked, but rather the final few questions people may want answered before they are ready to buy.<!--EndFragment--></p></div></div>
		</div>
	</div>
</div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-1687633c43a">
<div class="tve-page-section-out" data-css="tve-u-168763020ee"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-168763186ca"><div class="thrv_wrapper thrv-pricing-table" data-ct="pricing_table-25235" data-ct-name="I can&amp;#8217;t believe it&amp;#8217;s not PS 3-Column with toggle" data-css="tve-u-1687113dbe8" id="Pricing">
<div class="thrv_wrapper thrv-button-group tcb-no-clone tcb-no-delete tve_no_drag tcb-no-save" data-price-button-group="tve-u-price-button-group-164d1086276" data-css="tve-u-16cdd4016a6"><div class="thrv_wrapper thrv-button-group-item tcb-no-clone tcb-no-delete tve_no_drag tcb-no-title tcb-no-save tcb-active-state" data-instance="1762587" data-selector="[data-price-button-group=&quot;tve-u-price-button-group-164d1086276&quot;] .thrv-button-group-item.tcb-active-state" data-default="true">
<a href="#" class="tcb-button-link">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-1687f819282">One Time Payment</span></span>
</a>
</div><div class="thrv_wrapper thrv-button-group-item tcb-no-clone tcb-no-delete tve_no_drag tcb-no-title tcb-no-save" data-instance="93776" data-selector="[data-price-button-group=&quot;tve-u-price-button-group-164d1086276&quot;] .thrv-button-group-item:not(.tcb-active-state)" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text"><strong>3 m​onth instalments</strong></span></span>
</a>
</div></div>
<div class="tcb-flex-row tcb-pricing-table-box-container tcb--cols--2" data-instance="1762587" style="" data-css="tve-u-1687f815778"><div class="tcb-flex-col" data-label="Regular">
<div class="tcb-col dynamic-group-jzwlgcfd">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwl8slr" data-css="tve-u-1687154e0a3">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-1687120859b">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl8i79" data-css="tve-u-1687f7bb943">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f7bfc52"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwl37p0" data-css="tve-u-1687f7c429b" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f7bfc54"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl2lsa" data-css="tve-u-16cdd1d6041"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-light" data-name="">
<path d="M356 160H188c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12zm12 52v-8c0-6.6-5.4-12-12-12H188c-6.6 0-12 5.4-12 12v8c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12zm64.7 268h3.3c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H80c-44.2 0-80-35.8-80-80V80C0 35.8 35.8 0 80 0h344c13.3 0 24 10.7 24 24v368c0 10-6.2 18.6-14.9 22.2-3.6 16.1-4.4 45.6-.4 65.8zM128 384h288V32H128v352zm-96 16c13.4-10 30-16 48-16h16V32H80c-26.5 0-48 21.5-48 48v320zm372.3 80c-3.1-20.4-2.9-45.2 0-64H80c-64 0-64 64 0 64h324.3z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl45el" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f84a601" style=""><strong>regular course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl8252" data-tag="p" data-css="tve-u-168715544f9"><p class="tcb-pt-card-title" data-css="tve-u-16afda1a88b" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8b9223">
<div class="tve-content-box-background" data-css="tve-u-16cd86513b2"></div>
<div class="tve-cb" data-css="tve-u-16afd71f91e"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwl3vj0" data-css="tve-u-16afd729923"><p data-css="tve-u-16afd728298" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwl3jno" data-css="tve-u-16870f982b2"><p data-css="tve-u-1687f79fe5c" style="">247</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl7qja" data-css="tve-u-168803f9277">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f7a26b5"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-16880410cf7">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
</ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs dynamic-group-jzwl4lwe" style="" data-tcb_hover_state_parent="" data-css="tve-u-16cd87a29a7" data-button-style="tcb-global-button-tpl_jzvb5fzs">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext">Enroll Now</span></span>
</a>
</div>
</div>
</div>
</div>
</div><div class="tcb-flex-col" data-label="Advanced">
<div class="tcb-col dynamic-group-jzwlgcfd">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwl8slr" data-css="tve-u-1687117842d">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-16870fac08c">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl8i79" data-css="tve-u-1687f7bcd9c">
<div class="tve-content-box-background"></div>
<div class="tve-cb"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwl37p0" data-css="tve-u-1687f7d87cc" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f7da7a7"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl2lsa" data-css="tve-u-16cdd1d6b26"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-heart-light" data-name="">
<path d="M230.1 307.9c5.5 5.5 14.3 5.5 19.8 0l84.7-85.6c24.6-24.9 23.2-66.1-4.3-89.1-13.9-11.6-47.7-28.5-90.2 14.5-42.6-43-76.4-26.1-90.2-14.5-27.5 23-28.9 64.2-4.3 89.1l84.5 85.6zm-59.8-150.1c13.7-11.5 31.2-3.4 38.4 3.7l31.4 31.7 31.4-31.7c7.1-7.2 24.6-15.2 38.4-3.7 14.4 12 12.3 31.6 2.1 42l-72 72.6-71.8-72.6c-10.3-10.4-12.3-30 2.1-42zM448 392V24c0-13.3-10.7-24-24-24H80C35.8 0 0 35.8 0 80v352c0 44.2 35.8 80 80 80h356c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12h-3.3c-4-20.2-3.2-49.7.4-65.8 8.7-3.6 14.9-12.2 14.9-22.2zm-43.7 88H80c-64 0-64-64 0-64h324.3c-2.9 18.8-3.1 43.6 0 64zm11.7-96H80c-18 0-34.6 6-48 16V80c0-26.5 21.5-48 48-48h336v352z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl45el" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f84b879" style=""><strong>Advanced course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwl8252" data-tag="p" data-css="tve-u-16871554d8a"><p class="tcb-pt-card-title" data-css="tve-u-16afda1bffc" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8ba79b">
<div class="tve-content-box-background" data-css="tve-u-16cd85ce7b3"></div>
<div class="tve-cb" data-css="tve-u-16afd721aa0"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwl3vj0" data-css="tve-u-16afd72a869"><p data-css="tve-u-16afd7ba114" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwl3jno" data-css="tve-u-16870f982b2"><p data-css="tve-u-1687f7a10bc" style="">347</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwl7qja" data-css="tve-u-1687f83b5f1">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f7a5106"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-1687f83cf2c">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d0cb6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">X number of bonuses</span></li><li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d14b5"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">Access to Private Facebook Group</span></li><li class="thrv-styled-list-item dynamic-group-jzwl77zp" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwl6vlq" data-css="tve-u-168711d16a6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwl6565" data-css="tve-u-16871070f7d">30 minute private coaching call</span></li></ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right tcb-global-button-tpl_jzvb5fzs dynamic-group-jzwl4lwe" style="" data-tcb_hover_state_parent="" data-css="tve-u-16cd87a3e8f" data-button-style="tcb-global-button-tpl_jzvb5fzs">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text tcb-global-button-tpl_jzvb5fzs-prtext">Enroll Now</span></span>
</a>
</div>
</div>
</div></div>
</div></div>
<div class="tcb-flex-row tcb-pricing-table-box-container tcb--cols--2 tcb-permanently-hidden" data-instance="93776" style="" data-css="tve-u-1687f8a5583"><div class="tcb-flex-col" data-label="Regular Course">
<div class="tcb-col dynamic-group-jzwlfr20">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwlfckf" data-css="tve-u-1687102d21e">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-1687120859b">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlbfyu" data-css="tve-u-1687f8766a4">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16afd6cefc7"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwla73x" data-css="tve-u-1687f878881" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f87a4f0"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl9myh" data-css="tve-u-16afd69334d"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-light" data-name="">
<path d="M356 160H188c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12zm12 52v-8c0-6.6-5.4-12-12-12H188c-6.6 0-12 5.4-12 12v8c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12zm64.7 268h3.3c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H80c-44.2 0-80-35.8-80-80V80C0 35.8 35.8 0 80 0h344c13.3 0 24 10.7 24 24v368c0 10-6.2 18.6-14.9 22.2-3.6 16.1-4.4 45.6-.4 65.8zM128 384h288V32H128v352zm-96 16c13.4-10 30-16 48-16h16V32H80c-26.5 0-48 21.5-48 48v320zm372.3 80c-3.1-20.4-2.9-45.2 0-64H80c-64 0-64 64 0 64h324.3z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlast5" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f8a1e21" style=""><strong>regular course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlb6q1" data-tag="p" data-css="tve-u-1687136478c"><p class="tcb-pt-card-title" data-css="tve-u-16afda1feff" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8b0b71">
<div class="tve-content-box-background" data-css="tve-u-1687101c151"></div>
<div class="tve-cb" data-css="tve-u-16afd6d290f"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlch73" data-css="tve-u-1687f8989d0"><p data-css="tve-u-16afd6fe129" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwlbugq" data-css="tve-u-16afd70189b"><p data-css="tve-u-1687f891462" style="">99</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlcuzb" data-css="tve-u-1687143ac9b"><p data-css="tve-u-1687f893db3" style="">/month</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlf27m" data-css="tve-u-1688041ecc9">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f8a872c"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-16871034e8b">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
</ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right dynamic-group-jzwleqrz" data-css="tve-u-168712ff61d" style="" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-16afd76e217">Enroll Now</span></span>
</a>
</div>
</div>
</div>
</div>
</div><div class="tcb-flex-col" data-label="Advanced Course">
<div class="tcb-col dynamic-group-jzwlfr20">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-pt-cb-wrapper dynamic-group-jzwlfckf" data-css="tve-u-1687117842d">
<div class="tve-content-box-background tcb-pt-card" data-css="tve-u-16871087173"></div>
<div class="tve-cb tcb-pt-card-content" data-css="tve-u-16870fac08c">
<div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlbfyu" data-css="tve-u-1687f8774bd">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-16afd6d00dd"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tve_evt_manager_listen tve_et_tve-viewport tve_ea_thrive_animation tve_anim_fade_in dynamic-group-jzwla73x" data-css="tve-u-1687f887406" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;tve-viewport&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;fade_in&quot;,&quot;loop&quot;:0},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__" style="">
<div class="tve-content-box-background" data-css="tve-u-16870fc21b6"></div>
<div class="tve-cb" data-css="tve-u-1687f887d89"><div class="thrv_wrapper thrv_icon tcb-icon-display dynamic-group-jzwl9myh" data-css="tve-u-16afd4756b1"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-book-heart-light" data-name="">
<path d="M230.1 307.9c5.5 5.5 14.3 5.5 19.8 0l84.7-85.6c24.6-24.9 23.2-66.1-4.3-89.1-13.9-11.6-47.7-28.5-90.2 14.5-42.6-43-76.4-26.1-90.2-14.5-27.5 23-28.9 64.2-4.3 89.1l84.5 85.6zm-59.8-150.1c13.7-11.5 31.2-3.4 38.4 3.7l31.4 31.7 31.4-31.7c7.1-7.2 24.6-15.2 38.4-3.7 14.4 12 12.3 31.6 2.1 42l-72 72.6-71.8-72.6c-10.3-10.4-12.3-30 2.1-42zM448 392V24c0-13.3-10.7-24-24-24H80C35.8 0 0 35.8 0 80v352c0 44.2 35.8 80 80 80h356c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12h-3.3c-4-20.2-3.2-49.7.4-65.8 8.7-3.6 14.9-12.2 14.9-22.2zm-43.7 88H80c-64 0-64-64 0-64h324.3c-2.9 18.8-3.1 43.6 0 64zm11.7-96H80c-18 0-34.6 6-48 16V80c0-26.5 21.5-48 48-48h336v352z"></path>
</svg></div></div>
</div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlast5" data-tag="h5" data-css="tve-u-16870f982ae"><h5 class="tcb-pt-card-title" data-css="tve-u-1687f8a2b5b" style=""><strong>Advanced course</strong></h5></div><div class="thrv_wrapper thrv_text_element dynamic-group-jzwlb6q1" data-tag="p" data-css="tve-u-168713653ad"><p class="tcb-pt-card-title" data-css="tve-u-16afda1e9e5" style="">One sentence summary of what they get</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1687f8af4d6">
<div class="tve-content-box-background" data-css="tve-u-16871188546"></div>
<div class="tve-cb" data-css="tve-u-16afd6d3cd1"><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlch73" data-css="tve-u-1687f89a536"><p data-css="tve-u-16afd70405b" style="">$</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-value dynamic-group-jzwlbugq" data-css="tve-u-16afd70c8e5"><p data-css="tve-u-1687f892200" style="">127</p></div><div class="thrv_wrapper thrv_text_element tcb-pt-price tcb-pt-currency dynamic-group-jzwlcuzb" data-css="tve-u-1687143ac9b"><p data-css="tve-u-1687f895b34" style="">/month</p></div></div>
</div><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jzwlf27m" data-css="tve-u-168804269a6">
<div class="tve-content-box-background"></div>
<div class="tve-cb" data-css="tve-u-1687f8a750a"><div class="thrv_wrapper thrv-styled_list" data-icon-code="icon-check-light" data-css="tve-u-168804251a7">
<ul class="tcb-styled-list">
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b8"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Lifetime Access</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982b9"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Immediate Start</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style="">
<div class="tcb-styled-list-icon">
<div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-16870f982ba"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div>
</div>
<span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to All Modules</span>
</li>
<li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d0cb6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">X number of bonuses</span></li><li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d14b5"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">Access to Private Facebook Group</span></li><li class="thrv-styled-list-item dynamic-group-jzwledji" data-css="tve-u-1687107f8fa" style=""><div class="tcb-styled-list-icon"><div class="thrv_wrapper thrv_icon tve_no_drag tcb-no-delete tcb-no-clone tcb-no-save tcb-icon-inherit-style tcb-icon-display dynamic-group-jzwldz7j" data-css="tve-u-168711d16a6"><svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-check-light" data-name="">
<path d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
</svg></div></div><span class="thrv-advanced-inline-text tve_editable tcb-styled-list-icon-text tcb-no-delete tcb-no-save dynamic-group-jzwldfjw" data-css="tve-u-16871070f7d">30 minute private coaching call</span></li></ul>
</div></div>
</div>
<div class="tcb-pt-wrapper">
</div>
<div class="thrv_wrapper thrv-button tve-element-relative tve_ea_thrive_animation tve_anim_sweep_to_right dynamic-group-jzwleqrz" data-css="tve-u-168712ff621" style="" data-tcb_hover_state_parent="">
<a href="#" class="tcb-button-link tve_evt_manager_listen tve_et_mouseover" target="_blank" data-tcb-events="__TCB_EVENT_[{&quot;t&quot;:&quot;mouseover&quot;,&quot;config&quot;:{&quot;anim&quot;:&quot;sweep_to_right&quot;,&quot;loop&quot;:1},&quot;a&quot;:&quot;thrive_animation&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-16afd76d41d">Enroll Now</span></span>
</a>
</div>
</div>
</div></div>
</div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-1687632ab34">
<div class="tve-page-section-out" data-css="tve-u-16876335c6b"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1000 78" preserveAspectRatio="none" decoration-type="waves" class="svg-shape-top" data-css="tve-u-16876507f6e"><path d="M0,0v60c51.1-27,123.1-42,216-45c122-4,207.4,27.3,443,38c72.6,3.3,186.3-4.4,341-23V0H0z"></path><path opacity="0.6" d="M1,1v60c23.1-17,81.1-27,174-30c122-4,169.4,32.3,405,43c72.6,3.3,213-11,421-43V1H1z"></path><path opacity="0.2" d="M1,0v62c17.8-9,73.1-15,166-18c122-4,188,18,366,18c62,0,147.7-9,314-9     c32.1,0,83.4,6,154,18V0H1z"></path></svg></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-16876328b50"><div class="thrv_wrapper thrv-columns" data-css="tve-u-1687657f04c"><div class="tcb-flex-row tcb-resized tcb--cols--2" data-css="tve-u-1687654f810"><div class="tcb-flex-col" data-css="tve-u-1687656792f" style=""><div class="tcb-col"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-168f1ae761f">
	<div class="tve-content-box-background" data-css="tve-u-168f1aedcdd"></div>
	<div class="tve-cb" data-css="tve-u-168f1ae978b"></div>
</div></div></div><div class="tcb-flex-col" data-css="tve-u-168765679c5" style=""><div class="tcb-col" data-css="tve-u-1687657bff3"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687658b4c6" data-tag="h3"><h3 class="" data-css="tve-u-16876578220" style=""><strong>Your personal note</strong></h3></div><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687a380183"><p style=""><!--StartFragment-->This is your final chance to offer a personalised encouragement to your visitor. Warmly explain that this course is something you are proud of and that you hope to they’ll sign up so you can help them overcome their problems and get that amazing result!<!--EndFragment--></p></div><div class="thrv_wrapper tve_image_caption" data-css="tve-u-1687a3830e6"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-32358" alt="" width="336" height="222" title="Online_Course_Signature" data-id="32358" src="{tcb_lp_base_url}/css/images/Online_Course_Signature-1.png" style=""></span></div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section tve-custom-menu-inside" data-css="tve-u-16876699db1" style="">
<div class="tve-page-section-out" data-css="tve-u-1687669bc0d"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1687a3c29a7" style=""><div class="thrv_wrapper thrv-columns tve-custom-menu-inside" data-css="tve-u-1687a3e638b"><div class="tcb-flex-row tcb-resized tcb-medium-no-wrap v-2 m-edit tcb-mobile-wrap tcb--cols--2" data-css="tve-u-1687a3e8a4a"><div class="tcb-flex-col" data-css="tve-u-168766ac0f1" style=""><div class="tcb-col" data-css="tve-u-168842e0bd0"><div class="thrv_wrapper thrv_text_element" data-css="tve-u-1687aab1acf"><p data-css="tve-u-1687aa22247" style=""><strong><span class="thrive-shortcode-content" data-attr-id="1" data-extra_key="1" data-shortcode="thrive_global_fields" data-shortcode-name="[Company] Company Name">[thrive_global_fields id='1']</span></strong></p></div></div></div><div class="tcb-flex-col" data-css="tve-u-168766ac1a2" style=""><div class="tcb-col" style="" data-css="tve-u-168842e55d0"><div class="thrv_wrapper thrv_text_element"><p data-css="tve-u-16cdd90b391" style=""><span data-css="tve-u-16cdd909cc7"><span class="thrive-shortcode-content" data-attr-id="8" data-extra_key="2" data-shortcode="thrive_global_fields" data-shortcode-name="[Legal] Terms and Conditions">[thrive_global_fields id='8']</span></span>&nbsp; &nbsp;| &nbsp;<span class="thrive-shortcode-content" data-attr-id="6" data-extra_key="2" data-shortcode="thrive_global_fields" data-shortcode-name="[Legal] Privacy policy">[thrive_global_fields id='6']</span>&nbsp; &nbsp;| &nbsp; <span class="thrive-shortcode-content" data-attr-id="7" data-extra_key="2" data-shortcode="thrive_global_fields" data-shortcode-name="[Legal] Disclaimer">[thrive_global_fields id='7']</span>&nbsp; &nbsp;| &nbsp;&nbsp;<span class="thrive-shortcode-content" data-attr-id="9" data-extra_key="2" data-shortcode="thrive_global_fields" data-shortcode-name="[Legal] Contact">[thrive_global_fields id='9']</span></p></div></div></div></div></div></div>
</div>





















