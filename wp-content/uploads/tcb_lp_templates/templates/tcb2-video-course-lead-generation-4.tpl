<div class="thrv_wrapper thrv-page-section" data-css="tve-u-15dbe949f04" style="">
<div class="tve-page-section-out" data-css="tve-u-15dbe833ba3"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-15dbe85dae5"><div class="thrv_wrapper tve_image_caption" data-css="tve-u-15de2539068"><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-404" alt="" width="158" height="72" title="videou-course-logo" data-id="404" src="{tcb_lp_base_url}/css/images/videou-course-logo.png" style="width: 100%;"></span></div><div class="thrv_wrapper thrv-divider" data-style="tve_sep-1" data-thickness="1" data-color="rgba(255, 255, 255, 0.14)" data-css="tve-u-15dbe992682">
<hr class="tve_sep tve_sep-1">
</div><div class="thrv_wrapper thrv_heading"><h1 data-css="tve-u-15de2dd0e01" style="text-align: center;">How to <strong>Attract Customers </strong>and<strong> Make Money </strong>with Online Video (4-Part Course)</h1></div><div class="thrv_wrapper thrv-divider" data-style="tve_sep-1" data-thickness="1" data-color="rgba(255, 255, 255, 0.14)" data-css="tve-u-15dc16a5419">
<hr class="tve_sep tve_sep-1">
</div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-15dbeb977c5">
<div class="tve-page-section-out" data-css="tve-u-15dbe9f39d1"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-15dbeb63045"><div class="tcb-clear" data-css="tve-u-15de268f3d0"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box tcb-mobile-hidden tcb-tablet-hidden" data-css="tve-u-15de268f3b7" data-float="1">
<div class="tve-content-box-background" data-css="tve-u-15de2694203"></div>
<div class="tve-cb tve_empty_dropzone" data-css="tve-u-15de26905ad"><div class="thrv_wrapper thrv_icon tcb-icon-display" data-css="tve-u-15de269bff2"><svg class="tcb-icon" viewBox="0 0 28 28" data-name="chevron-down">
<title>chevron-down</title>
<path d="M26.297 12.625l-11.594 11.578c-0.391 0.391-1.016 0.391-1.406 0l-11.594-11.578c-0.391-0.391-0.391-1.031 0-1.422l2.594-2.578c0.391-0.391 1.016-0.391 1.406 0l8.297 8.297 8.297-8.297c0.391-0.391 1.016-0.391 1.406 0l2.594 2.578c0.391 0.391 0.391 1.031 0 1.422z"></path>
</svg></div></div>
</div></div><div class="thrv_wrapper thrv-columns" data-css="tve-u-15de258b281"><div class="tcb-flex-row tcb-resized tcb--cols--2 tcb-medium-wrap" data-css="tve-u-15de27c5cb1"><div class="tcb-flex-col" data-css="tve-u-15de257402e" style=""><div class="tcb-col"><div class="tcb-clear" data-css="tve-u-15de254de84"><div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-15dbea06df8">
<div class="tve-content-box-background" data-css="tve-u-15dbea10e64"></div>
<div class="tve-cb tve_empty_dropzone"><div class="thrv_responsive_video thrv_wrapper rv_style_lifted_style4" data-type="youtube" data-url="https://youtu.be/XzwqYMzRWpo" data-showinfo="0" data-rel="0" data-modestbranding="1" data-css="tve-u-15dbea33e0a">
<div class="tve_responsive_video_container">
<div class="video_overlay"></div>
<iframe data-code="XzwqYMzRWpo" data-provider="youtube" src="https://www.youtube.com/embed/XzwqYMzRWpo?rel=0&amp;modestbranding=1&amp;controls=1&amp;showinfo=0&amp;fs=1&amp;wmode=transparent" data-src="https://www.youtube.com/embed/XzwqYMzRWpo?rel=0&amp;modestbranding=1&amp;controls=1&amp;showinfo=0&amp;fs=1&amp;wmode=transparent" frameborder="0" allowfullscreen=""></iframe></div>
</div></div>
</div></div></div></div><div class="tcb-flex-col" data-css="tve-u-15de2574064" style=""><div class="tcb-col tve_empty_dropzone" data-css="tve-u-15de275a62c"><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" data-css="tve-u-15de263f166"><p data-css="tve-u-15de2583247"><span style="font-size: 25px;"><strong><span style="color: rgb(255, 255, 255);">Crush it With Online Video!</span></strong></span></p></div><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" data-css="tve-u-15de2e08b8c"><p data-css="tve-u-15de2634fa5"><span style="color: rgb(255, 255, 255); font-size: 18px;">Video is the most captivating and most effective medium for communicating online. Now it's time for you to benefit from it.</span></p></div><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" data-css="tve-u-15de2713573"><p data-css="tve-u-15de27193ff"><span style="color: rgb(51, 51, 51); font-size: 20px;">Click the button below and <u><strong>get started</strong> <strong>for FREE</strong></u> right away!</span></p></div><div class="thrv_wrapper thrv-button tve_ea_thrive_lightbox" data-css="tve-u-15de273167a">
<a href="" class="tcb-button-link tve_evt_manager_listen tve_et_click" data-tcb-events="__TCB_EVENT_[{&quot;config&quot;:{&quot;l_anim&quot;:&quot;slide_top&quot;,&quot;l_id&quot;:3689},&quot;a&quot;:&quot;thrive_lightbox&quot;,&quot;t&quot;:&quot;click&quot;}]_TNEVE_BCT__">
<span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-15df06b09d6"><strong>start the free course</strong></span></span>
</a>
</div></div></div></div></div></div>
</div><div class="thrv_wrapper thrv-page-section" data-css="tve-u-15dbed94414">
<div class="tve-page-section-out" data-css="tve-u-15dbed9108a"></div>
<div class="tve-page-section-in tve_empty_dropzone"><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" data-css="tve-u-15dbeda4e61"><p data-css="tve-u-15dbedb23bb" style="text-align: center;"><span style="color: rgb(153, 153, 153); font-size: 16px;">{tcb_current_year} Thrive Landing Pages. All rights Reserved | </span><span style="color: rgb(255, 255, 255);"><a href="#"><span style="font-size: 16px;"><u>Disclaimer</u></span></a></span></p></div></div>
</div>